
package servicioweb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the servicioweb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Ingresar_QNAME = new QName("http://ServicioWeb/", "ingresar");
    private final static QName _IngresarResponse_QNAME = new QName("http://ServicioWeb/", "ingresarResponse");
    private final static QName _RecuperarID_QNAME = new QName("http://ServicioWeb/", "recuperarID");
    private final static QName _RecuperarIDResponse_QNAME = new QName("http://ServicioWeb/", "recuperarIDResponse");
    private final static QName _RecuperarRut_QNAME = new QName("http://ServicioWeb/", "recuperarRut");
    private final static QName _RecuperarRutResponse_QNAME = new QName("http://ServicioWeb/", "recuperarRutResponse");
    private final static QName _RecuperarTipo_QNAME = new QName("http://ServicioWeb/", "recuperarTipo");
    private final static QName _RecuperarTipoResponse_QNAME = new QName("http://ServicioWeb/", "recuperarTipoResponse");
    private final static QName _RecuperarTipoUsu_QNAME = new QName("http://ServicioWeb/", "recuperarTipoUsu");
    private final static QName _RecuperarTipoUsuResponse_QNAME = new QName("http://ServicioWeb/", "recuperarTipoUsuResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: servicioweb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Ingresar }
     * 
     */
    public Ingresar createIngresar() {
        return new Ingresar();
    }

    /**
     * Create an instance of {@link IngresarResponse }
     * 
     */
    public IngresarResponse createIngresarResponse() {
        return new IngresarResponse();
    }

    /**
     * Create an instance of {@link RecuperarID }
     * 
     */
    public RecuperarID createRecuperarID() {
        return new RecuperarID();
    }

    /**
     * Create an instance of {@link RecuperarIDResponse }
     * 
     */
    public RecuperarIDResponse createRecuperarIDResponse() {
        return new RecuperarIDResponse();
    }

    /**
     * Create an instance of {@link RecuperarRut }
     * 
     */
    public RecuperarRut createRecuperarRut() {
        return new RecuperarRut();
    }

    /**
     * Create an instance of {@link RecuperarRutResponse }
     * 
     */
    public RecuperarRutResponse createRecuperarRutResponse() {
        return new RecuperarRutResponse();
    }

    /**
     * Create an instance of {@link RecuperarTipo }
     * 
     */
    public RecuperarTipo createRecuperarTipo() {
        return new RecuperarTipo();
    }

    /**
     * Create an instance of {@link RecuperarTipoResponse }
     * 
     */
    public RecuperarTipoResponse createRecuperarTipoResponse() {
        return new RecuperarTipoResponse();
    }

    /**
     * Create an instance of {@link RecuperarTipoUsu }
     * 
     */
    public RecuperarTipoUsu createRecuperarTipoUsu() {
        return new RecuperarTipoUsu();
    }

    /**
     * Create an instance of {@link RecuperarTipoUsuResponse }
     * 
     */
    public RecuperarTipoUsuResponse createRecuperarTipoUsuResponse() {
        return new RecuperarTipoUsuResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ingresar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "ingresar")
    public JAXBElement<Ingresar> createIngresar(Ingresar value) {
        return new JAXBElement<Ingresar>(_Ingresar_QNAME, Ingresar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IngresarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "ingresarResponse")
    public JAXBElement<IngresarResponse> createIngresarResponse(IngresarResponse value) {
        return new JAXBElement<IngresarResponse>(_IngresarResponse_QNAME, IngresarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarID")
    public JAXBElement<RecuperarID> createRecuperarID(RecuperarID value) {
        return new JAXBElement<RecuperarID>(_RecuperarID_QNAME, RecuperarID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarIDResponse")
    public JAXBElement<RecuperarIDResponse> createRecuperarIDResponse(RecuperarIDResponse value) {
        return new JAXBElement<RecuperarIDResponse>(_RecuperarIDResponse_QNAME, RecuperarIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarRut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarRut")
    public JAXBElement<RecuperarRut> createRecuperarRut(RecuperarRut value) {
        return new JAXBElement<RecuperarRut>(_RecuperarRut_QNAME, RecuperarRut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarRutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarRutResponse")
    public JAXBElement<RecuperarRutResponse> createRecuperarRutResponse(RecuperarRutResponse value) {
        return new JAXBElement<RecuperarRutResponse>(_RecuperarRutResponse_QNAME, RecuperarRutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarTipo")
    public JAXBElement<RecuperarTipo> createRecuperarTipo(RecuperarTipo value) {
        return new JAXBElement<RecuperarTipo>(_RecuperarTipo_QNAME, RecuperarTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarTipoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarTipoResponse")
    public JAXBElement<RecuperarTipoResponse> createRecuperarTipoResponse(RecuperarTipoResponse value) {
        return new JAXBElement<RecuperarTipoResponse>(_RecuperarTipoResponse_QNAME, RecuperarTipoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarTipoUsu }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarTipoUsu")
    public JAXBElement<RecuperarTipoUsu> createRecuperarTipoUsu(RecuperarTipoUsu value) {
        return new JAXBElement<RecuperarTipoUsu>(_RecuperarTipoUsu_QNAME, RecuperarTipoUsu.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarTipoUsuResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarTipoUsuResponse")
    public JAXBElement<RecuperarTipoUsuResponse> createRecuperarTipoUsuResponse(RecuperarTipoUsuResponse value) {
        return new JAXBElement<RecuperarTipoUsuResponse>(_RecuperarTipoUsuResponse_QNAME, RecuperarTipoUsuResponse.class, null, value);
    }

}
