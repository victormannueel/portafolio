
package servicioweb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para reserva complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="reserva"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fechaIngreso" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="fechaSalida" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="idAbono" type="{http://ServicioWeb/}abono" minOccurs="0"/&gt;
 *         &lt;element name="idDepartamento" type="{http://ServicioWeb/}departamento" minOccurs="0"/&gt;
 *         &lt;element name="idEstadoReserva" type="{http://ServicioWeb/}estadoReserva" minOccurs="0"/&gt;
 *         &lt;element name="idReserva" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="idServicioExtra" type="{http://ServicioWeb/}servicioExtra" minOccurs="0"/&gt;
 *         &lt;element name="numAcompanantes" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="rutUsuario" type="{http://ServicioWeb/}usuario" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reserva", propOrder = {
    "fechaIngreso",
    "fechaSalida",
    "idAbono",
    "idDepartamento",
    "idEstadoReserva",
    "idReserva",
    "idServicioExtra",
    "numAcompanantes",
    "rutUsuario"
})
public class Reserva_Type {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaIngreso;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaSalida;
    protected Abono idAbono;
    protected Departamento idDepartamento;
    protected EstadoReserva idEstadoReserva;
    protected int idReserva;
    protected ServicioExtra idServicioExtra;
    protected int numAcompanantes;
    protected Usuario rutUsuario;

    /**
     * Obtiene el valor de la propiedad fechaIngreso.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * Define el valor de la propiedad fechaIngreso.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaIngreso(XMLGregorianCalendar value) {
        this.fechaIngreso = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaSalida.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaSalida() {
        return fechaSalida;
    }

    /**
     * Define el valor de la propiedad fechaSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaSalida(XMLGregorianCalendar value) {
        this.fechaSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad idAbono.
     * 
     * @return
     *     possible object is
     *     {@link Abono }
     *     
     */
    public Abono getIdAbono() {
        return idAbono;
    }

    /**
     * Define el valor de la propiedad idAbono.
     * 
     * @param value
     *     allowed object is
     *     {@link Abono }
     *     
     */
    public void setIdAbono(Abono value) {
        this.idAbono = value;
    }

    /**
     * Obtiene el valor de la propiedad idDepartamento.
     * 
     * @return
     *     possible object is
     *     {@link Departamento }
     *     
     */
    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    /**
     * Define el valor de la propiedad idDepartamento.
     * 
     * @param value
     *     allowed object is
     *     {@link Departamento }
     *     
     */
    public void setIdDepartamento(Departamento value) {
        this.idDepartamento = value;
    }

    /**
     * Obtiene el valor de la propiedad idEstadoReserva.
     * 
     * @return
     *     possible object is
     *     {@link EstadoReserva }
     *     
     */
    public EstadoReserva getIdEstadoReserva() {
        return idEstadoReserva;
    }

    /**
     * Define el valor de la propiedad idEstadoReserva.
     * 
     * @param value
     *     allowed object is
     *     {@link EstadoReserva }
     *     
     */
    public void setIdEstadoReserva(EstadoReserva value) {
        this.idEstadoReserva = value;
    }

    /**
     * Obtiene el valor de la propiedad idReserva.
     * 
     */
    public int getIdReserva() {
        return idReserva;
    }

    /**
     * Define el valor de la propiedad idReserva.
     * 
     */
    public void setIdReserva(int value) {
        this.idReserva = value;
    }

    /**
     * Obtiene el valor de la propiedad idServicioExtra.
     * 
     * @return
     *     possible object is
     *     {@link ServicioExtra }
     *     
     */
    public ServicioExtra getIdServicioExtra() {
        return idServicioExtra;
    }

    /**
     * Define el valor de la propiedad idServicioExtra.
     * 
     * @param value
     *     allowed object is
     *     {@link ServicioExtra }
     *     
     */
    public void setIdServicioExtra(ServicioExtra value) {
        this.idServicioExtra = value;
    }

    /**
     * Obtiene el valor de la propiedad numAcompanantes.
     * 
     */
    public int getNumAcompanantes() {
        return numAcompanantes;
    }

    /**
     * Define el valor de la propiedad numAcompanantes.
     * 
     */
    public void setNumAcompanantes(int value) {
        this.numAcompanantes = value;
    }

    /**
     * Obtiene el valor de la propiedad rutUsuario.
     * 
     * @return
     *     possible object is
     *     {@link Usuario }
     *     
     */
    public Usuario getRutUsuario() {
        return rutUsuario;
    }

    /**
     * Define el valor de la propiedad rutUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link Usuario }
     *     
     */
    public void setRutUsuario(Usuario value) {
        this.rutUsuario = value;
    }

}
