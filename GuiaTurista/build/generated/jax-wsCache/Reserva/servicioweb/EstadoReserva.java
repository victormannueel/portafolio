
package servicioweb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para estadoReserva complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="estadoReserva"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="descipcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idEstadoReserva" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "estadoReserva", propOrder = {
    "descipcion",
    "idEstadoReserva"
})
public class EstadoReserva {

    protected String descipcion;
    protected int idEstadoReserva;

    /**
     * Obtiene el valor de la propiedad descipcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescipcion() {
        return descipcion;
    }

    /**
     * Define el valor de la propiedad descipcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescipcion(String value) {
        this.descipcion = value;
    }

    /**
     * Obtiene el valor de la propiedad idEstadoReserva.
     * 
     */
    public int getIdEstadoReserva() {
        return idEstadoReserva;
    }

    /**
     * Define el valor de la propiedad idEstadoReserva.
     * 
     */
    public void setIdEstadoReserva(int value) {
        this.idEstadoReserva = value;
    }

}
