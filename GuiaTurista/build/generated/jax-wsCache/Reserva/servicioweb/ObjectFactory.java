
package servicioweb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the servicioweb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Abono_QNAME = new QName("http://ServicioWeb/", "abono");
    private final static QName _AbonoReserva_QNAME = new QName("http://ServicioWeb/", "abonoReserva");
    private final static QName _AbonoReservaResponse_QNAME = new QName("http://ServicioWeb/", "abonoReservaResponse");
    private final static QName _Ciudad_QNAME = new QName("http://ServicioWeb/", "ciudad");
    private final static QName _Comuna_QNAME = new QName("http://ServicioWeb/", "comuna");
    private final static QName _Departamento_QNAME = new QName("http://ServicioWeb/", "departamento");
    private final static QName _Disponibilidad_QNAME = new QName("http://ServicioWeb/", "disponibilidad");
    private final static QName _Estado_QNAME = new QName("http://ServicioWeb/", "estado");
    private final static QName _EstadoReserva_QNAME = new QName("http://ServicioWeb/", "estadoReserva");
    private final static QName _EstadoReservaA_QNAME = new QName("http://ServicioWeb/", "estadoReservaA");
    private final static QName _EstadoReservaAResponse_QNAME = new QName("http://ServicioWeb/", "estadoReservaAResponse");
    private final static QName _EstadoReservaFinal_QNAME = new QName("http://ServicioWeb/", "estadoReservaFinal");
    private final static QName _EstadoReservaFinalResponse_QNAME = new QName("http://ServicioWeb/", "estadoReservaFinalResponse");
    private final static QName _MostrarDepaPorId_QNAME = new QName("http://ServicioWeb/", "mostrarDepaPorId");
    private final static QName _MostrarDepaPorIdResponse_QNAME = new QName("http://ServicioWeb/", "mostrarDepaPorIdResponse");
    private final static QName _MostrarReservaFinal_QNAME = new QName("http://ServicioWeb/", "mostrarReservaFinal");
    private final static QName _MostrarReservaFinalResponse_QNAME = new QName("http://ServicioWeb/", "mostrarReservaFinalResponse");
    private final static QName _RecuperarNumeroReserva_QNAME = new QName("http://ServicioWeb/", "recuperarNumeroReserva");
    private final static QName _RecuperarNumeroReservaResponse_QNAME = new QName("http://ServicioWeb/", "recuperarNumeroReservaResponse");
    private final static QName _Region_QNAME = new QName("http://ServicioWeb/", "region");
    private final static QName _Reserva_QNAME = new QName("http://ServicioWeb/", "reserva");
    private final static QName _ServicioExtra_QNAME = new QName("http://ServicioWeb/", "servicioExtra");
    private final static QName _TipoPago_QNAME = new QName("http://ServicioWeb/", "tipoPago");
    private final static QName _Usuario_QNAME = new QName("http://ServicioWeb/", "usuario");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: servicioweb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Abono }
     * 
     */
    public Abono createAbono() {
        return new Abono();
    }

    /**
     * Create an instance of {@link AbonoReserva }
     * 
     */
    public AbonoReserva createAbonoReserva() {
        return new AbonoReserva();
    }

    /**
     * Create an instance of {@link AbonoReservaResponse }
     * 
     */
    public AbonoReservaResponse createAbonoReservaResponse() {
        return new AbonoReservaResponse();
    }

    /**
     * Create an instance of {@link Ciudad }
     * 
     */
    public Ciudad createCiudad() {
        return new Ciudad();
    }

    /**
     * Create an instance of {@link Comuna }
     * 
     */
    public Comuna createComuna() {
        return new Comuna();
    }

    /**
     * Create an instance of {@link Departamento }
     * 
     */
    public Departamento createDepartamento() {
        return new Departamento();
    }

    /**
     * Create an instance of {@link Disponibilidad }
     * 
     */
    public Disponibilidad createDisponibilidad() {
        return new Disponibilidad();
    }

    /**
     * Create an instance of {@link Estado }
     * 
     */
    public Estado createEstado() {
        return new Estado();
    }

    /**
     * Create an instance of {@link EstadoReserva }
     * 
     */
    public EstadoReserva createEstadoReserva() {
        return new EstadoReserva();
    }

    /**
     * Create an instance of {@link EstadoReservaA }
     * 
     */
    public EstadoReservaA createEstadoReservaA() {
        return new EstadoReservaA();
    }

    /**
     * Create an instance of {@link EstadoReservaAResponse }
     * 
     */
    public EstadoReservaAResponse createEstadoReservaAResponse() {
        return new EstadoReservaAResponse();
    }

    /**
     * Create an instance of {@link EstadoReservaFinal }
     * 
     */
    public EstadoReservaFinal createEstadoReservaFinal() {
        return new EstadoReservaFinal();
    }

    /**
     * Create an instance of {@link EstadoReservaFinalResponse }
     * 
     */
    public EstadoReservaFinalResponse createEstadoReservaFinalResponse() {
        return new EstadoReservaFinalResponse();
    }

    /**
     * Create an instance of {@link MostrarDepaPorId }
     * 
     */
    public MostrarDepaPorId createMostrarDepaPorId() {
        return new MostrarDepaPorId();
    }

    /**
     * Create an instance of {@link MostrarDepaPorIdResponse }
     * 
     */
    public MostrarDepaPorIdResponse createMostrarDepaPorIdResponse() {
        return new MostrarDepaPorIdResponse();
    }

    /**
     * Create an instance of {@link MostrarReservaFinal }
     * 
     */
    public MostrarReservaFinal createMostrarReservaFinal() {
        return new MostrarReservaFinal();
    }

    /**
     * Create an instance of {@link MostrarReservaFinalResponse }
     * 
     */
    public MostrarReservaFinalResponse createMostrarReservaFinalResponse() {
        return new MostrarReservaFinalResponse();
    }

    /**
     * Create an instance of {@link RecuperarNumeroReserva }
     * 
     */
    public RecuperarNumeroReserva createRecuperarNumeroReserva() {
        return new RecuperarNumeroReserva();
    }

    /**
     * Create an instance of {@link RecuperarNumeroReservaResponse }
     * 
     */
    public RecuperarNumeroReservaResponse createRecuperarNumeroReservaResponse() {
        return new RecuperarNumeroReservaResponse();
    }

    /**
     * Create an instance of {@link Region }
     * 
     */
    public Region createRegion() {
        return new Region();
    }

    /**
     * Create an instance of {@link Reserva_Type }
     * 
     */
    public Reserva_Type createReserva_Type() {
        return new Reserva_Type();
    }

    /**
     * Create an instance of {@link ServicioExtra }
     * 
     */
    public ServicioExtra createServicioExtra() {
        return new ServicioExtra();
    }

    /**
     * Create an instance of {@link TipoPago }
     * 
     */
    public TipoPago createTipoPago() {
        return new TipoPago();
    }

    /**
     * Create an instance of {@link Usuario }
     * 
     */
    public Usuario createUsuario() {
        return new Usuario();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Abono }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "abono")
    public JAXBElement<Abono> createAbono(Abono value) {
        return new JAXBElement<Abono>(_Abono_QNAME, Abono.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbonoReserva }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "abonoReserva")
    public JAXBElement<AbonoReserva> createAbonoReserva(AbonoReserva value) {
        return new JAXBElement<AbonoReserva>(_AbonoReserva_QNAME, AbonoReserva.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AbonoReservaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "abonoReservaResponse")
    public JAXBElement<AbonoReservaResponse> createAbonoReservaResponse(AbonoReservaResponse value) {
        return new JAXBElement<AbonoReservaResponse>(_AbonoReservaResponse_QNAME, AbonoReservaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ciudad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "ciudad")
    public JAXBElement<Ciudad> createCiudad(Ciudad value) {
        return new JAXBElement<Ciudad>(_Ciudad_QNAME, Ciudad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Comuna }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "comuna")
    public JAXBElement<Comuna> createComuna(Comuna value) {
        return new JAXBElement<Comuna>(_Comuna_QNAME, Comuna.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Departamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "departamento")
    public JAXBElement<Departamento> createDepartamento(Departamento value) {
        return new JAXBElement<Departamento>(_Departamento_QNAME, Departamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Disponibilidad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "disponibilidad")
    public JAXBElement<Disponibilidad> createDisponibilidad(Disponibilidad value) {
        return new JAXBElement<Disponibilidad>(_Disponibilidad_QNAME, Disponibilidad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Estado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "estado")
    public JAXBElement<Estado> createEstado(Estado value) {
        return new JAXBElement<Estado>(_Estado_QNAME, Estado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstadoReserva }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "estadoReserva")
    public JAXBElement<EstadoReserva> createEstadoReserva(EstadoReserva value) {
        return new JAXBElement<EstadoReserva>(_EstadoReserva_QNAME, EstadoReserva.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstadoReservaA }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "estadoReservaA")
    public JAXBElement<EstadoReservaA> createEstadoReservaA(EstadoReservaA value) {
        return new JAXBElement<EstadoReservaA>(_EstadoReservaA_QNAME, EstadoReservaA.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstadoReservaAResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "estadoReservaAResponse")
    public JAXBElement<EstadoReservaAResponse> createEstadoReservaAResponse(EstadoReservaAResponse value) {
        return new JAXBElement<EstadoReservaAResponse>(_EstadoReservaAResponse_QNAME, EstadoReservaAResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstadoReservaFinal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "estadoReservaFinal")
    public JAXBElement<EstadoReservaFinal> createEstadoReservaFinal(EstadoReservaFinal value) {
        return new JAXBElement<EstadoReservaFinal>(_EstadoReservaFinal_QNAME, EstadoReservaFinal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstadoReservaFinalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "estadoReservaFinalResponse")
    public JAXBElement<EstadoReservaFinalResponse> createEstadoReservaFinalResponse(EstadoReservaFinalResponse value) {
        return new JAXBElement<EstadoReservaFinalResponse>(_EstadoReservaFinalResponse_QNAME, EstadoReservaFinalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarDepaPorId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "mostrarDepaPorId")
    public JAXBElement<MostrarDepaPorId> createMostrarDepaPorId(MostrarDepaPorId value) {
        return new JAXBElement<MostrarDepaPorId>(_MostrarDepaPorId_QNAME, MostrarDepaPorId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarDepaPorIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "mostrarDepaPorIdResponse")
    public JAXBElement<MostrarDepaPorIdResponse> createMostrarDepaPorIdResponse(MostrarDepaPorIdResponse value) {
        return new JAXBElement<MostrarDepaPorIdResponse>(_MostrarDepaPorIdResponse_QNAME, MostrarDepaPorIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarReservaFinal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "mostrarReservaFinal")
    public JAXBElement<MostrarReservaFinal> createMostrarReservaFinal(MostrarReservaFinal value) {
        return new JAXBElement<MostrarReservaFinal>(_MostrarReservaFinal_QNAME, MostrarReservaFinal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MostrarReservaFinalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "mostrarReservaFinalResponse")
    public JAXBElement<MostrarReservaFinalResponse> createMostrarReservaFinalResponse(MostrarReservaFinalResponse value) {
        return new JAXBElement<MostrarReservaFinalResponse>(_MostrarReservaFinalResponse_QNAME, MostrarReservaFinalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarNumeroReserva }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarNumeroReserva")
    public JAXBElement<RecuperarNumeroReserva> createRecuperarNumeroReserva(RecuperarNumeroReserva value) {
        return new JAXBElement<RecuperarNumeroReserva>(_RecuperarNumeroReserva_QNAME, RecuperarNumeroReserva.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarNumeroReservaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "recuperarNumeroReservaResponse")
    public JAXBElement<RecuperarNumeroReservaResponse> createRecuperarNumeroReservaResponse(RecuperarNumeroReservaResponse value) {
        return new JAXBElement<RecuperarNumeroReservaResponse>(_RecuperarNumeroReservaResponse_QNAME, RecuperarNumeroReservaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Region }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "region")
    public JAXBElement<Region> createRegion(Region value) {
        return new JAXBElement<Region>(_Region_QNAME, Region.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Reserva_Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "reserva")
    public JAXBElement<Reserva_Type> createReserva(Reserva_Type value) {
        return new JAXBElement<Reserva_Type>(_Reserva_QNAME, Reserva_Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServicioExtra }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "servicioExtra")
    public JAXBElement<ServicioExtra> createServicioExtra(ServicioExtra value) {
        return new JAXBElement<ServicioExtra>(_ServicioExtra_QNAME, ServicioExtra.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoPago }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "tipoPago")
    public JAXBElement<TipoPago> createTipoPago(TipoPago value) {
        return new JAXBElement<TipoPago>(_TipoPago_QNAME, TipoPago.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Usuario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "usuario")
    public JAXBElement<Usuario> createUsuario(Usuario value) {
        return new JAXBElement<Usuario>(_Usuario_QNAME, Usuario.class, null, value);
    }

}
