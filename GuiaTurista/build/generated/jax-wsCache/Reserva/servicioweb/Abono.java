
package servicioweb;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para abono complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="abono"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idAbono" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="idTipoPago" type="{http://ServicioWeb/}tipoPago" minOccurs="0"/&gt;
 *         &lt;element name="precio" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abono", propOrder = {
    "idAbono",
    "idTipoPago",
    "precio"
})
public class Abono {

    protected BigDecimal idAbono;
    protected TipoPago idTipoPago;
    protected BigInteger precio;

    /**
     * Obtiene el valor de la propiedad idAbono.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIdAbono() {
        return idAbono;
    }

    /**
     * Define el valor de la propiedad idAbono.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIdAbono(BigDecimal value) {
        this.idAbono = value;
    }

    /**
     * Obtiene el valor de la propiedad idTipoPago.
     * 
     * @return
     *     possible object is
     *     {@link TipoPago }
     *     
     */
    public TipoPago getIdTipoPago() {
        return idTipoPago;
    }

    /**
     * Define el valor de la propiedad idTipoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPago }
     *     
     */
    public void setIdTipoPago(TipoPago value) {
        this.idTipoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad precio.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPrecio() {
        return precio;
    }

    /**
     * Define el valor de la propiedad precio.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPrecio(BigInteger value) {
        this.precio = value;
    }

}
