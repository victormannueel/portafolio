
package servicioweb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para pagotarjeta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="pagotarjeta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rut" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numero_tarjeta" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="numero_seguridad" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="precio" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pagotarjeta", propOrder = {
    "rut",
    "numeroTarjeta",
    "numeroSeguridad",
    "precio"
})
public class Pagotarjeta {

    protected String rut;
    @XmlElement(name = "numero_tarjeta")
    protected int numeroTarjeta;
    @XmlElement(name = "numero_seguridad")
    protected int numeroSeguridad;
    protected int precio;

    /**
     * Obtiene el valor de la propiedad rut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRut() {
        return rut;
    }

    /**
     * Define el valor de la propiedad rut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRut(String value) {
        this.rut = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroTarjeta.
     * 
     */
    public int getNumeroTarjeta() {
        return numeroTarjeta;
    }

    /**
     * Define el valor de la propiedad numeroTarjeta.
     * 
     */
    public void setNumeroTarjeta(int value) {
        this.numeroTarjeta = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSeguridad.
     * 
     */
    public int getNumeroSeguridad() {
        return numeroSeguridad;
    }

    /**
     * Define el valor de la propiedad numeroSeguridad.
     * 
     */
    public void setNumeroSeguridad(int value) {
        this.numeroSeguridad = value;
    }

    /**
     * Obtiene el valor de la propiedad precio.
     * 
     */
    public int getPrecio() {
        return precio;
    }

    /**
     * Define el valor de la propiedad precio.
     * 
     */
    public void setPrecio(int value) {
        this.precio = value;
    }

}
