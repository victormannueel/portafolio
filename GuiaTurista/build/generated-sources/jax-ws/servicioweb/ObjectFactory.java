
package servicioweb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the servicioweb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Pagotarjeta_QNAME = new QName("http://ServicioWeb/", "pagotarjeta");
    private final static QName _PagotarjetaResponse_QNAME = new QName("http://ServicioWeb/", "pagotarjetaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: servicioweb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Pagotarjeta }
     * 
     */
    public Pagotarjeta createPagotarjeta() {
        return new Pagotarjeta();
    }

    /**
     * Create an instance of {@link PagotarjetaResponse }
     * 
     */
    public PagotarjetaResponse createPagotarjetaResponse() {
        return new PagotarjetaResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Pagotarjeta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "pagotarjeta")
    public JAXBElement<Pagotarjeta> createPagotarjeta(Pagotarjeta value) {
        return new JAXBElement<Pagotarjeta>(_Pagotarjeta_QNAME, Pagotarjeta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PagotarjetaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ServicioWeb/", name = "pagotarjetaResponse")
    public JAXBElement<PagotarjetaResponse> createPagotarjetaResponse(PagotarjetaResponse value) {
        return new JAXBElement<PagotarjetaResponse>(_PagotarjetaResponse_QNAME, PagotarjetaResponse.class, null, value);
    }

}
