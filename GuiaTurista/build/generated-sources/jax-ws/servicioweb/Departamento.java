
package servicioweb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para departamento complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="departamento"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cantHabitaciones" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="direccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idCiudad" type="{http://ServicioWeb/}ciudad" minOccurs="0"/&gt;
 *         &lt;element name="idComuna" type="{http://ServicioWeb/}comuna" minOccurs="0"/&gt;
 *         &lt;element name="idDepartamento" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="idDisponibilidad" type="{http://ServicioWeb/}disponibilidad" minOccurs="0"/&gt;
 *         &lt;element name="idEstado" type="{http://ServicioWeb/}estado" minOccurs="0"/&gt;
 *         &lt;element name="idRegion" type="{http://ServicioWeb/}region" minOccurs="0"/&gt;
 *         &lt;element name="imagen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numHabitacion" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="precioNoche" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "departamento", propOrder = {
    "cantHabitaciones",
    "descripcion",
    "direccion",
    "idCiudad",
    "idComuna",
    "idDepartamento",
    "idDisponibilidad",
    "idEstado",
    "idRegion",
    "imagen",
    "nombre",
    "numHabitacion",
    "precioNoche"
})
public class Departamento {

    protected int cantHabitaciones;
    protected String descripcion;
    protected String direccion;
    protected Ciudad idCiudad;
    protected Comuna idComuna;
    protected int idDepartamento;
    protected Disponibilidad idDisponibilidad;
    protected Estado idEstado;
    protected Region idRegion;
    protected String imagen;
    protected String nombre;
    protected int numHabitacion;
    protected int precioNoche;

    /**
     * Obtiene el valor de la propiedad cantHabitaciones.
     * 
     */
    public int getCantHabitaciones() {
        return cantHabitaciones;
    }

    /**
     * Define el valor de la propiedad cantHabitaciones.
     * 
     */
    public void setCantHabitaciones(int value) {
        this.cantHabitaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Define el valor de la propiedad descripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad direccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Define el valor de la propiedad direccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccion(String value) {
        this.direccion = value;
    }

    /**
     * Obtiene el valor de la propiedad idCiudad.
     * 
     * @return
     *     possible object is
     *     {@link Ciudad }
     *     
     */
    public Ciudad getIdCiudad() {
        return idCiudad;
    }

    /**
     * Define el valor de la propiedad idCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link Ciudad }
     *     
     */
    public void setIdCiudad(Ciudad value) {
        this.idCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad idComuna.
     * 
     * @return
     *     possible object is
     *     {@link Comuna }
     *     
     */
    public Comuna getIdComuna() {
        return idComuna;
    }

    /**
     * Define el valor de la propiedad idComuna.
     * 
     * @param value
     *     allowed object is
     *     {@link Comuna }
     *     
     */
    public void setIdComuna(Comuna value) {
        this.idComuna = value;
    }

    /**
     * Obtiene el valor de la propiedad idDepartamento.
     * 
     */
    public int getIdDepartamento() {
        return idDepartamento;
    }

    /**
     * Define el valor de la propiedad idDepartamento.
     * 
     */
    public void setIdDepartamento(int value) {
        this.idDepartamento = value;
    }

    /**
     * Obtiene el valor de la propiedad idDisponibilidad.
     * 
     * @return
     *     possible object is
     *     {@link Disponibilidad }
     *     
     */
    public Disponibilidad getIdDisponibilidad() {
        return idDisponibilidad;
    }

    /**
     * Define el valor de la propiedad idDisponibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link Disponibilidad }
     *     
     */
    public void setIdDisponibilidad(Disponibilidad value) {
        this.idDisponibilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad idEstado.
     * 
     * @return
     *     possible object is
     *     {@link Estado }
     *     
     */
    public Estado getIdEstado() {
        return idEstado;
    }

    /**
     * Define el valor de la propiedad idEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link Estado }
     *     
     */
    public void setIdEstado(Estado value) {
        this.idEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad idRegion.
     * 
     * @return
     *     possible object is
     *     {@link Region }
     *     
     */
    public Region getIdRegion() {
        return idRegion;
    }

    /**
     * Define el valor de la propiedad idRegion.
     * 
     * @param value
     *     allowed object is
     *     {@link Region }
     *     
     */
    public void setIdRegion(Region value) {
        this.idRegion = value;
    }

    /**
     * Obtiene el valor de la propiedad imagen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * Define el valor de la propiedad imagen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImagen(String value) {
        this.imagen = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad numHabitacion.
     * 
     */
    public int getNumHabitacion() {
        return numHabitacion;
    }

    /**
     * Define el valor de la propiedad numHabitacion.
     * 
     */
    public void setNumHabitacion(int value) {
        this.numHabitacion = value;
    }

    /**
     * Obtiene el valor de la propiedad precioNoche.
     * 
     */
    public int getPrecioNoche() {
        return precioNoche;
    }

    /**
     * Define el valor de la propiedad precioNoche.
     * 
     */
    public void setPrecioNoche(int value) {
        this.precioNoche = value;
    }

}
