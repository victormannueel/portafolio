
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Gran Turismo</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href="layout/styles/layoutL.css" rel="stylesheet" type="text/css" media="all">
    </head>
    <body id="top">
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- Top Background Image Wrapper -->
        <div class="bgded overlay light" style="background-image:url('images/demo/backgrounds/01.jpeg');"> 
            <!-- ################################################################################################ -->
            <div class="wrapper row0">
                <div id="topbar" class="hoc clear"> 
                    <!-- ################################################################################################ -->
                    <ul class="nospace">
                        <li><a href="menuPrincipal.jsp"><i class="fa fa-lg fa-home"></i></a></li>
                        <li><a href="cerrarSesion.jsp" title="Cerrar sesion"><i class="fa fa-lg fa-edit"></i></a></li>
                    </ul>
                    <!-- ################################################################################################ -->
                </div>
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div class="wrapper row1">
                <header id="header" class="hoc clear"> 
                    <!-- ################################################################################################ -->
                    <div id="logo" class="fl_left">
                        <h1> 
                            <a href="menuPrincipal.jsp">Gran Turismo</a></h1>
                    </div>
                    <nav id="mainav" class="fl_right">
                        <ul class="clear">
                            <li class="active"><a href="menuPrincipal.jsp">Home</a></li>
                            <li><a class="drop" href="#">¡Reserva ahora!</a>
                                <ul>
                                    <li><a href="departamentosDisponibles.jsp">Departamentos</a></li>
                                    <li><a href="listaReservas.jsp">Tus Reservas</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- ################################################################################################ -->
                </header>
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div id="pageintro" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <article>
                    <h3 class="heading">Gran Turismo</h3>
                    <p>Bienvenido! ${user}</p>
                    <img src="images/logo.png" alt=""width="10%"/>
                    <footer>
                        <ul class="nospace inline pushright">
                            <li><a class="btn" href="listaReservas.jsp">Tus reservas</a></li>
                            <li><a class="btn inverse" href="departamentosDisponibles.jsp">Departamentos</a></li>
                        </ul>
                    </footer>
                </article>
                <!-- ################################################################################################ -->
            </div>
            <!-- ################################################################################################ -->
        </div>
        <!-- End Top Background Image Wrapper -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row3">
            <main class="hoc container clear"> 
                <!-- main body -->
                <!-- ################################################################################################ -->
                <div class="sectiontitle">
                    <h6 class="heading">Gran Turismo</h6>
                    <p>Bienvenido a Gran Turismo, te ayudamos en tus viajes con los mejores punto de descanso</p>
                </div>
                <article class="group">
                    <div class="one_half first">
                        <h6 class="heading">Ubicados en los mejores lugares turiscos</h6>
                        <p>No te pierdas la oportunidad de arrendar tu departamento en el nuevo sistema de Turismo Real</p>
                    </div>
                    <div class="one_half"><a class="imgover" href="#"><img src="images/demo/480x360.jpg" alt=""></a></div>
                </article>
                <!-- ################################################################################################ -->
                <!-- / main body -->
                <div class="clear"></div>
            </main>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row2 bgded overlay" style="background-image:url('images/demo/backgrounds/02.jpg');">
            <section class="hoc container clear"> 
                <!-- ################################################################################################ -->
                <div class="sectiontitle">
                    <h6 class="heading">Nos comprometemos con: </h6>
                </div>
                <ul class="nospace group services">
                    <li class="one_quarter first"><i class="fa fa-4x fa-chain-broken btmspace-30"></i>
                        <h6 class="heading font-x1">Reserva</h6>
                        <ul class="nospace">
                            <li><i class="fa fa-link"></i> <a>De manera facil</a></li>
                        </ul>
                    </li>
                    <li class="one_quarter"><i class="fa fa-4x fa-expeditedssl btmspace-30"></i>
                        <h6 class="heading font-x1">Reserva</h6>
                        <ul class="nospace">
                            <li><i class="fa fa-link"></i> <a>De manera segura</a></li>
                        </ul>
                    </li>
                    <li class="one_quarter"><i class="fa fa-4x fa-hourglass-2 btmspace-30"></i>
                        <h6 class="heading font-x1">Reserva</h6>
                        <ul class="nospace">
                            <li><i class="fa fa-link"></i> <a >De forma rapida</a></li>
                        </ul>
                    </li>
                    <li class="one_quarter"><i class="fa fa-4x fa-snowflake-o btmspace-30"></i>
                        <h6 class="heading font-x1">Reserva</h6>
                        <ul class="nospace">
                            <li><i class="fa fa-link"></i> <a>Con precios imperdibles </a></li>
                        </ul>
                    </li>
                </ul>
                <footer class="center"><a class="btn" href="listaReservas.jsp">¡Reserva ahora! &raquo;</a></footer>
                <!-- ################################################################################################ -->
            </section>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row3">
            <figure class="hoc container clear clients"> 
                <!-- ################################################################################################ -->

                <!-- ################################################################################################ -->
            </figure>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row2 bgded overlay" style="background-image:url('images/demo/backgrounds/03.jpg');">
            <section class="hoc container clear"> 
                <!-- ################################################################################################ -->
                <div class="sectiontitle">
                    <h6 class="heading">Departamentos</h6>
                    <p>Ve nuestra gran variedad de departamentos que Gran Turismo</p>
                </div>
                <div class="group team">
                    <figure class="one_quarter first"><a class="imgover" href="#"><img src="images/1.jpeg" src="mifoto.jpg" width="100" height="100"/></a>
                    </figure>
                    <figure class="one_quarter"><a class="imgover" href="#"><img src="images/Departamento2.jpg" src="mifoto.jpg" width="100" height="100"/></a>
                    </figure>
                    <figure class="one_quarter"><a class="imgover" href="#"><img src="images/3.jpg" src="mifoto.jpg" width="100" height="100"/></a>
                    </figure>
                    <figure class="one_quarter"><a class="imgover" href="#"> <img src="images/4.jpg" src="mifoto.jpg" width="100" height="100"/></a>
                    </figure>
                </div>
                <footer class="center"><a class="btn" href="departamentosDisponibles.jsp">Departamentos &raquo;</a></footer>
                <!-- ################################################################################################ -->
            </section>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row4 ">
            <footer id="footer" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <article class="one_quarter first">
                    <h6 class="heading">Gran turismo</h6>
                    <p>Gran Turismo, empresa que sirve a la comunidad su principal objetivo es dar a sus cliente una experiencia inolvidable en las reservas que hagan a traves de la nueva plataforma "Gran Turismo"</p>
                </article>
                <div class="one_quarter">
                    <h6 class="heading">Contactanos</h6>
                    <ul class="nospace btmspace-30 linklist contact">
                        <li><i class="fa fa-map-marker"></i>
                            <address>
                                Pasaje la armonica &amp; 01385, Puente alto, Santiago
                            </address>
                        </li>
                        <li><i class="fa fa-phone"></i> +56(9) 32140169</li>
                        <li><i class="fa fa-envelope-o"></i> granturismo@gmail.com</li>
                    </ul>
                </div>
                <!-- ################################################################################################ -->
            </footer>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row5">
            <div id="copyright" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <p class="fl_left">Copyright &copy; 2019 - All Rights Reserved</p>
                <!-- ################################################################################################ -->
            </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
        <!-- JAVASCRIPTS -->
        <script src="layout/scripts/jqueryL.min.js"></script>
        <script src="layout/scripts/jqueryL.backtotop.js"></script>
        <script src="layout/scripts/jqueryL.mobilemenu.js"></script>
    </body>
</html>
