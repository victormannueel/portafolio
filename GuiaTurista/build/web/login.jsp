<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <sql:setDataSource var="dataSource" driver="oracle.jdbc.OracleDriver" url="jdbc:oracle:thin:@localhost:1521:XE" user="portafolio" password="portafolio"></sql:setDataSource>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="UTF-8">

    <style>
        @import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
        @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);

        body{
            margin: 0;
            padding: 0;
            background: #fff;
            background-size: cover;

            color: #fff;
            font-family: Arial;
            font-size: 12px;
        }

        .body{
            position: absolute;
            top: -20px;
            left: -20px;
            right: -40px;
            bottom: -40px;
            width: auto;
            height: auto;
            background-image: url(http://ginva.com/wp-content/uploads/2012/07/city-skyline-wallpapers-008.jpg);
            background-size: cover;
            -webkit-filter: blur(5px);
            z-index: 0;
        }

        .grad{
            position: absolute;
            top: -20px;
            left: -20px;
            right: -40px;
            bottom: -40px;
            width: auto;
            height: auto;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
            z-index: 1;
            opacity: 0.7;
        }

        .header{
            position: absolute;
            top: calc(50% - 35px);
            left: calc(50% - 255px);
            z-index: 2;
        }

        .header div{
            float: left;
            color: #fff;
            font-family: 'Exo', sans-serif;
            font-size: 35px;
            font-weight: 200;
        }

        .header div span{
            color: #5379fa !important;
        }
        .box {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%,-50%);
            width: 400px;
            padding: 40px;
            background: rgba(0,0,0,.8);
            box-sizing: border-box;
            box-shadow: 0 15px 25px rgba(0,0,0,.5);
            border-radius: 10px;
        }
        .box .avatar {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            position: absolute;
            top: -50px;
            left: calc(50% - 50px);
        }
        .box h2{
            margin: 0 0 30px;
            padding: 0;
            color: #fff;
            text-align: center;
        }
        .box .inputBox{
            position: relative;
        }
        .box .inputBox input{
            width: 100%;
            padding: 10px 0;
            font-size: 16px;
            color: #fff;
            letter-spacing: 1px;
            margin-bottom: 30px;
            border: none;
            border-bottom: 1px solid #fff;
            outline: none;
            background: transparent;
        }
        .box .inputBox label {
            position: absolute;
            top: 0;
            left: 0;
            padding: 10px 0;
            font-size: 16px;
            color: #fff;
            pointer-events: none;
            transition: .5s;
        }
        .box .inputBox input:focus ~ label,
        .box .inputBox input:valid ~ label
        {
            top: -20px;
            left: 0;
            color: #03a9f4;
            font-size: 12px;
        }
        .box input[type="submit"]{
            width: 100%;
            background: transparent;
            border: none;
            outline: none;
            color: #fff;
            background: #03a9f4;
            padding: 10px 20px;
            cursor: pointer;
            border-radius: 5px;
            margin-top: 5%;
        }
        .box .inputBox i{
            position: absolute;
            top: 0;
            right: 0;
            padding: 10px 0;
            font-size: 16px;
            color: #fff;
            pointer-events: none;
            transition: .5s;
        }
        .box .inputBox a{
            position: absolute;
            top: 0;
            right: 0;
            text-align: right;
        }
    </style>
    <script src="js/prefixfree.min.js"></script>
    <script src="https://kit.fontawesome.com/663d87ba20.js" crossorigin="anonymous"></script>
</head>
<div class="body"></div>
<body>
    <div class="box">
        <img class="avatar" src="images/logo.png">
        <h2>Iniciar Sesión</h2>
    <form action="login" method="POST">
        <div class="inputBox">
            <i class="fas fa-user"></i>
            <input type="text" name="txtUsuario" id="txtUsuario"  minlength="1" maxlength="10" required="">
            <label>Username</label>
        </div>
        <div class="inputBox">
            <i class="fas fa-lock"></i>
            <input type="password" name="txtPass" id="txtPass"  minlength="1" maxlength="20" required="">
          <label>Password</label>
        </div>
        <div>
            <select name="tipoUsu" id="tipoUsu" class="form-control" style="width: 100%;">
                <option>Seleccione tipo usuario</option>
                <option value="2">Funcionario</option>
                <option value="3">Cliente</option>
            </select>
        </div>
            <div>
                <input type="submit" name="btnAccion"  value="Ingresar">
            </div>
        <br>
            <c:forEach var="item" items="${error}">
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> ${item}
                </div>
            </c:forEach>
        <br>
        <div>
        <a href="MantenedorUsuario/registrarUsuario.jsp">Registrate</a>
        </div>
    </form>
    </div>
    <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
</body>
</html>
