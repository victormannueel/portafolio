
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Turismo Real | Ingreso</title>
    </head>
    <body>
        <div class="jumbotron text-center">
            <input type="button" onclick="history.back()" name="volver atrás" value="volver atrás" class="btn btn-info">
            <h1><FONT FACE="impact" color="orange">Metodo de pago</FONT></h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <c:forEach var="item" items="${lista}">
                        <table>
                            <h2>Datos clientes</h2>
                            <tr>
                                <td>Rut: </td>
                                <td> ${item.rutUsuario}</td>
                            </tr>
                            <tr>
                                <td>Nombre: </td>
                                <td> ${item.nombre} ${item.apellido}</td>
                            </tr>
                            <tr>
                                <td>Email: </td>
                                <td>${item.email}</td>
                            </tr>

                        </table>
                    </c:forEach>

                </div>
                <div class="col-sm-8">
                    <form action="tipopago" method="POST">
                        <table class="table table-striped">
                            <tr>
                                <td>Metodo de pago </td>
                                <td>
                                    <select class="form-control" id="sel1" name="cbo">
                                        <option hidden="0" value="cbo">--Seleccione--</option>
                                        <option value="1">Tarjeta</option>
                                        <option value="2">Tranferencia</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Monto abono: </td>
                                <td>
                                    <input type="text" name="txtPagoReserva" class="form-control" required min="0" max="2000000" disabled="" value="$ 20.000">
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button name="btnAccion" value="GuardarIN" id="GuardarIN" class="btn btn-success">Pagar</button>
                                </td>
                            </tr>
                            <tr>

                            </tr>
                            <tr>
                                <td>Rut del Asociado: </td>
                                <td><input type="text" name="txtReserva" class="form-control"  value="${idRut}" disabled=""></td>
                            </tr>
                        </table>
                        <center>
                            <h1> Total estimado: $${total}</h1>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
