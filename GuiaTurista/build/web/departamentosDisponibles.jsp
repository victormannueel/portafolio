
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>Gran Turismo | Departamentos </title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href="layout/styles/layoutL.css" rel="stylesheet" type="text/css" media="all">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- Top Background Image Wrapper -->
        <div class="bgded overlay light" style="background-image:url('images/demo/backgrounds/01.jpeg');"> 
            <!-- ################################################################################################ -->
            <div class="wrapper row0">
                <div id="topbar" class="hoc clear"> 
                    <!-- ################################################################################################ -->
                    <ul class="nospace">
                        <li><a href="menuPrincipal.jsp"><i class="fa fa-lg fa-home"></i></a></li>
                        <li><a href="cerrarSesion.jsp" title="Cerrar sesion"><i class="fa fa-lg fa-edit"></i></a></li>
                    </ul>
                    <!-- ################################################################################################ -->
                </div>
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div class="wrapper row1">
                <header id="header" class="hoc clear"> 
                    <!-- ################################################################################################ -->
                    <div id="logo" class="fl_left">
                        <h1><a href="menuPrincipal.jsp">Gran Turismo</a></h1>
                    </div>
                    <nav id="mainav" class="fl_right">
                        <ul class="clear">
                            <li><a href="menuPrincipal.jsp">Home</a></li>
                            <li class="active"><a class="drop" href="#">¡Reserva Ahora!</a>
                                <ul>
                                    <li class="active"><a href="departamentosDisponibles.jsp">Departamentos</a></li>
                                    <li><a href="listaReservas.jsp">Tus Reservas</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- ################################################################################################ -->
                </header>
            </div>
            <div id="pageintro" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <article>
                    <p>Guia Del Turista</p>
                    <h3 class="heading">Turismo Real</h3>
                    <p>Bienvenido! ${user}</p>
                    <footer>
                        <ul class="nospace inline pushright">
                            <form action="departamento" method="POST">
                                <input type="submit" name="btnAccion" value="Mostrar Departamentos" class="btn btn-success">
                            </form>
                        </ul>
                    </footer>
                </article>
                <!-- ################################################################################################ -->
            </div>
            <div id="breadcrumb" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <ul>
                    <li><a href="menuPrincipal.jsp">Home</a></li>
                    <li><a href="departamentosDisponibles.jsp">Departamentos</a></li>
                </ul>
                <!-- ################################################################################################ -->
            </div>

            <!-- ################################################################################################ -->
        </div>
        <!-- End Top Background Image Wrapper -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row3">
            <main class="hoc container clear"> 
                <!-- main body -->
                <!-- ################################################################################################ -->
                <div class="content"> 
                    <!-- ################################################################################################ -->
                    <c:forEach var="item" items="${lista}">
                        <div id="gallery">
                            <figure>
                                <header class="heading">Departamentos</header>
                                <ul class="nospace clear">
                                    <div class="row">

                                        <table>
                                            <tr>
                                                <td>
                                            <li class="one_quarter first"><a type="submit" href="ServletDepa"><img src="images/${item.getImagen()}" style="width: 25%;"></a></li>
                                            <form action="negocio" method="POST">
                                                <input type="submit" name="btnAccion" value="Reservar" class="btn btn-success">
                                                <input type="hidden" name="idDepa" id="idDepa" value="${item.getIdDepartamento()}">
                                                <input type="hidden" name="idUser" id="idDepa" value="${rut}">
                                            </form>
                                            </td>
                                            </tr>
                                        </table>

                                        <div class="col-sm-4" >
                                            <li style="color: black; font: 200% sans-serif" >Departamento: ${item.getNombre()}</li>
                                            <li  style="color: black;">Descripcion: ${item.getDescripcion()}</li>
                                            <li style="color: black;">Cantidad de habitaciones: ${item.getCantHabitaciones()}</li>
                                            <li style="color: black;">Direccion: ${item.getDireccion()}</li>
                                            <li style="color: black;">Region: ${item.getIdRegion().descripcion}</li>
                                            <li style="color: black;">Estado:  ${item.getIdEstado().descripcion}</li>
                                        </div>
                                        <div class="col-sm-4" >
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <li  style="color: black;">Numero habitacion: ${item.getNumHabitacion()}</li>
                                            <li style="color: black">Precio Noche: $${item.getPrecioNoche()}</li>
                                            <li style="color: black">Ciudad: ${item.getIdCiudad().descripcion}</li>
                                            <li style="color: black">Comuna: ${item.getIdComuna().descripcion}</li>
                                            <li style="color: black">Estado: ${item.getIdDisponibilidad().descripcion}</li>
                                        </div>
                                    </div>
                                </ul>
                            </figure>
                        </div>
                    </c:forEach>
                </div>
                <!-- ################################################################################################ -->
                <!-- / main body -->
                <div class="clear"></div>
            </main>
        </div>
    </body>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="wrapper row4 ">
        <footer id="footer" class="hoc clear"> 
            <!-- ################################################################################################ -->
            <article class="one_quarter first">
                <h6 class="heading">Tursmo Real</h6>
                <p>Fundada en el año 2019</p>
                <p>Turismo Real, empresa que sirve a la comunidad su principal objetivo es dar a sus cliente una experiencia inolvidable en las reservas que hagan a travez de la nueva plataforma "Guia del turista"</p>
            </article>
            <div class="one_quarter">
                <h6 class="heading">Contactanos</h6>
                <ul class="nospace btmspace-30 linklist contact">
                    <li><i class="fa fa-map-marker"></i>
                        <address>
                            Pasaje la armonica &amp; 01385, Puente alto, Santiago
                        </address>
                    </li>
                    <li><i class="fa fa-phone"></i> +56(9) 32140169</li>
                    <li><i class="fa fa-envelope-o"></i> turismoreal@gmail.com</li>
                </ul>
            </div>
            <!-- ################################################################################################ -->
        </footer>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <div class="wrapper row5">
        <div id="copyright" class="hoc clear"> 
            <!-- ################################################################################################ -->
            <p class="fl_left">Copyright &copy; 2019 - All Rights Reserved - <a>Kevin Llanquel</a></p>
            <!-- ################################################################################################ -->
        </div>
    </div>
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <!-- ################################################################################################ -->
    <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
    <!-- JAVASCRIPTS -->
    <script src="layout/scripts/jqueryL.min.js"></script>
    <script src="layout/scripts/jqueryL.backtotop.js"></script>
    <script src="layout/scripts/jqueryL.mobilemenu.js"></script>
</html>
