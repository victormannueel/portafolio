
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gran Turismo | Administracion</title>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="menuCheck.jsp">Gran Turismo | Administracion </a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="Funcionario/ingreso.jsp">Check IN</a></li>
                    <li><a href="Funcionario/salida.jsp">Check OUT</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> ver Perfil</a></li>
                    <li><a href="cerrarSesion.jsp"><span class="glyphicon glyphicon-log-in"></span> Cerrar Sesion</a></li>
                </ul>
            </div>
        </nav>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <center>
                    <img src="images/logo.png" alt=""width="50%"/>
                     <h1><FONT FACE="times new roman" color="black">Bienvenido ${user}!</FONT></h1>
                </center>
            </div>
            <div class="col-sm-4"></div>
        </div>

    </body>
</html>
