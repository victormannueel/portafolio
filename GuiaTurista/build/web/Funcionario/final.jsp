<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gran Turismo | Salida</title>
    </head>
    <body>  
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Gran Turismo | Salida </a>
                </div>
                <form class="navbar-form navbar-left" action="../checks">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Nº Reserva" name="txtId" id="txtId" required="">
                    </div>
                    <button type="submit" class="btn btn-warning" name="btnAccion" value="BuscarOUT">Buscar</button> 
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> ver Perfil</a></li>
                    <li><a href="../cerrarSesion.jsp"><span class="glyphicon glyphicon-log-in"></span> Cerrar Sesion</a></li>
                </ul>
            </div>
        </nav>
        <div class="jumbotron text-center">
            <input type="button" onclick="history.back()" name="volver atrás" value="volver atrás" class="btn btn-info">
            <h1><FONT FACE="impact" color="orange">CHECK OUT</FONT></h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <form action="../checks" method="POST">
                        <table class="table table-striped">
                            <tr>
                                <td>Multa: </td>
                                <td><input type="number" name="txtMulta" class="form-control" required min="0" max="2000000"></td>
                            </tr>
                            <tr>
                                <td>Costo reparacion: </td>
                                <td><input type="number" name="txtCostoReparacion" class="form-control" required min="0" max="2000000"></td>
                            </tr> 
                            <tr>
                                <td>Estado check: </td>
                                <td>
                                    <select class="form-control" id="sel1" name="cbo">
                                        <option hidden="" value="0">--Seleccione--</option>
                                        <option value="1">Aprobado</option>
                                        <option value="2">No apropado</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>N° Check de ingreso: </td>
                                <td><input type="number" name="txtCheckIn" class="form-control" disabled="" value="${idCheck}"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button name="btnAccion" value="GuardarOUT" id="GuardarIN" class="btn btn-success">Guardar</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Rut del Asociado: </td>
                                <td><input type="text" name="txtReserva" class="form-control"  value="${rutAsociado}" disabled=""></td>
                            </tr>
                        </table>
                    </form>
                    <c:forEach var="item" items="${error}">
                        <div class="alert alert-danger alert-dismissible fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Error!</strong> ${item}
                        </div>
                    </c:forEach>
                    <c:forEach var="item" items="${correcto}">
                        <div class="alert alert-success alert-dismissible fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Super!</strong> ${item}
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </body>
</html>
