/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "ESTADO_CHECK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoCheck.findAll", query = "SELECT e FROM EstadoCheck e")
    , @NamedQuery(name = "EstadoCheck.findByIdEstadoCheck", query = "SELECT e FROM EstadoCheck e WHERE e.idEstadoCheck = :idEstadoCheck")
    , @NamedQuery(name = "EstadoCheck.findByDescripcion", query = "SELECT e FROM EstadoCheck e WHERE e.descripcion = :descripcion")})
public class EstadoCheck implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ESTADO_CHECK")
    private int idEstadoCheck;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstadoCheck")
    private List<CheckOut> checkOutList;

    public EstadoCheck() {
    }

    public EstadoCheck(int idEstadoCheck) {
        this.idEstadoCheck = idEstadoCheck;
    }

    public EstadoCheck(int idEstadoCheck, String descripcion) {
        this.idEstadoCheck = idEstadoCheck;
        this.descripcion = descripcion;
    }

    public int getIdEstadoCheck() {
        return idEstadoCheck;
    }

    public void setIdEstadoCheck(int idEstadoCheck) {
        this.idEstadoCheck = idEstadoCheck;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<CheckOut> getCheckOutList() {
        return checkOutList;
    }

    public void setCheckOutList(List<CheckOut> checkOutList) {
        this.checkOutList = checkOutList;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idEstadoCheck != null ? idEstadoCheck.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof EstadoCheck)) {
//            return false;
//        }
//        EstadoCheck other = (EstadoCheck) object;
//        if ((this.idEstadoCheck == null && other.idEstadoCheck != null) || (this.idEstadoCheck != null && !this.idEstadoCheck.equals(other.idEstadoCheck))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.EstadoCheck[ idEstadoCheck=" + idEstadoCheck + " ]";
    }
    
}
