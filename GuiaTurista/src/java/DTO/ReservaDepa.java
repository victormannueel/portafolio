/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "RESERVA_DEPA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReservaDepa.findAll", query = "SELECT r FROM ReservaDepa r")
    , @NamedQuery(name = "ReservaDepa.findByIdReservaDepartamento", query = "SELECT r FROM ReservaDepa r WHERE r.idReservaDepartamento = :idReservaDepartamento")})
public class ReservaDepa implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_RESERVA_DEPARTAMENTO")
    private int idReservaDepartamento;
    @JoinColumn(name = "ID_DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")
    @ManyToOne(optional = false)
    private Departamento idDepartamento;
    @JoinColumn(name = "ID_RESERVA", referencedColumnName = "ID_RESERVA")
    @ManyToOne(optional = false)
    private Reserva idReserva;

    public ReservaDepa() {
    }

    public ReservaDepa(int idReservaDepartamento) {
        this.idReservaDepartamento = idReservaDepartamento;
    }

    public int getIdReservaDepartamento() {
        return idReservaDepartamento;
    }

    public void setIdReservaDepartamento(int idReservaDepartamento) {
        this.idReservaDepartamento = idReservaDepartamento;
    }

    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Departamento idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Reserva getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Reserva idReserva) {
        this.idReserva = idReserva;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idReservaDepartamento != null ? idReservaDepartamento.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ReservaDepa)) {
//            return false;
//        }
//        ReservaDepa other = (ReservaDepa) object;
//        if ((this.idReservaDepartamento == null && other.idReservaDepartamento != null) || (this.idReservaDepartamento != null && !this.idReservaDepartamento.equals(other.idReservaDepartamento))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.ReservaDepa[ idReservaDepartamento=" + idReservaDepartamento + " ]";
    }
    
}
