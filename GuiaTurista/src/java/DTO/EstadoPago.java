/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "ESTADO_PAGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoPago.findAll", query = "SELECT e FROM EstadoPago e")
    , @NamedQuery(name = "EstadoPago.findByIdEstadoPago", query = "SELECT e FROM EstadoPago e WHERE e.idEstadoPago = :idEstadoPago")
    , @NamedQuery(name = "EstadoPago.findByDescripcion", query = "SELECT e FROM EstadoPago e WHERE e.descripcion = :descripcion")})
public class EstadoPago implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ESTADO_PAGO")
    private int idEstadoPago;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstadopago")
    private List<Transferencia> transferenciaList;

    public EstadoPago() {
    }

    public EstadoPago(int idEstadoPago) {
        this.idEstadoPago = idEstadoPago;
    }

    public EstadoPago(int idEstadoPago, String descripcion) {
        this.idEstadoPago = idEstadoPago;
        this.descripcion = descripcion;
    }

    public int getIdEstadoPago() {
        return idEstadoPago;
    }

    public void setIdEstadoPago(int idEstadoPago) {
        this.idEstadoPago = idEstadoPago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Transferencia> getTransferenciaList() {
        return transferenciaList;
    }

    public void setTransferenciaList(List<Transferencia> transferenciaList) {
        this.transferenciaList = transferenciaList;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idEstadoPago != null ? idEstadoPago.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof EstadoPago)) {
//            return false;
//        }
//        EstadoPago other = (EstadoPago) object;
//        if ((this.idEstadoPago == null && other.idEstadoPago != null) || (this.idEstadoPago != null && !this.idEstadoPago.equals(other.idEstadoPago))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.EstadoPago[ idEstadoPago=" + idEstadoPago + " ]";
    }
    
}
