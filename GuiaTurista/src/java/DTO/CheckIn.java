/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "CHECK_IN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CheckIn.findAll", query = "SELECT c FROM CheckIn c")
    , @NamedQuery(name = "CheckIn.findByIdCheckIn", query = "SELECT c FROM CheckIn c WHERE c.idCheckIn = :idCheckIn")
    , @NamedQuery(name = "CheckIn.findByCliConfirmacion", query = "SELECT c FROM CheckIn c WHERE c.cliConfirmacion = :cliConfirmacion")
    , @NamedQuery(name = "CheckIn.findByPagoReserva", query = "SELECT c FROM CheckIn c WHERE c.pagoReserva = :pagoReserva")})
public class CheckIn implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CHECK_IN")
    private int idCheckIn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLI_CONFIRMACION")
    private Character cliConfirmacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PAGO_RESERVA")
    private int pagoReserva;
    @JoinColumn(name = "ID_RESERVA", referencedColumnName = "ID_RESERVA")
    @ManyToOne(optional = false)
    private Reserva idReserva;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCheckIn")
    private List<CheckOut> checkOutList;

    public CheckIn() {
    }

    public CheckIn(int idCheckIn) {
        this.idCheckIn = idCheckIn;
    }

    public CheckIn(Character cliConfirmacion, int pagoReserva) {
        this.cliConfirmacion = cliConfirmacion;
        this.pagoReserva = pagoReserva;
    }

    
    public CheckIn(int idCheckIn, Character cliConfirmacion, int pagoReserva) {
        this.idCheckIn = idCheckIn;
        this.cliConfirmacion = cliConfirmacion;
        this.pagoReserva = pagoReserva;
    }

    public int getIdCheckIn() {
        return idCheckIn;
    }

    public void setIdCheckIn(int idCheckIn) {
        this.idCheckIn = idCheckIn;
    }

    public Character getCliConfirmacion() {
        return cliConfirmacion;
    }

    public void setCliConfirmacion(Character cliConfirmacion) {
        this.cliConfirmacion = cliConfirmacion;
    }

    public int getPagoReserva() {
        return pagoReserva;
    }

    public void setPagoReserva(int pagoReserva) {
        this.pagoReserva = pagoReserva;
    }

    public Reserva getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Reserva idReserva) {
        this.idReserva = idReserva;
    }

    @XmlTransient
    public List<CheckOut> getCheckOutList() {
        return checkOutList;
    }

    public void setCheckOutList(List<CheckOut> checkOutList) {
        this.checkOutList = checkOutList;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idCheckIn != null ? idCheckIn.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof CheckIn)) {
//            return false;
//        }
//        CheckIn other = (CheckIn) object;
//        if ((this.idCheckIn == null && other.idCheckIn != null) || (this.idCheckIn != null && !this.idCheckIn.equals(other.idCheckIn))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.CheckIn[ idCheckIn=" + idCheckIn + " ]";
    }
    
}
