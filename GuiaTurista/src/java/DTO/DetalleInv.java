/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "DETALLE_INV")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleInv.findAll", query = "SELECT d FROM DetalleInv d")
    , @NamedQuery(name = "DetalleInv.findByIdDetalle", query = "SELECT d FROM DetalleInv d WHERE d.idDetalle = :idDetalle")
    , @NamedQuery(name = "DetalleInv.findByDescripcion", query = "SELECT d FROM DetalleInv d WHERE d.descripcion = :descripcion")
    , @NamedQuery(name = "DetalleInv.findByCantidad", query = "SELECT d FROM DetalleInv d WHERE d.cantidad = :cantidad")
    , @NamedQuery(name = "DetalleInv.findByPrecio", query = "SELECT d FROM DetalleInv d WHERE d.precio = :precio")})
public class DetalleInv implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_DETALLE")
    private int idDetalle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANTIDAD")
    private int cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO")
    private int precio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetalle")
    private List<Revision> revisionList;
    @JoinColumn(name = "ID_INVENTARIO", referencedColumnName = "ID_INVENTARIO")
    @ManyToOne(optional = false)
    private InvDepa idInventario;

    public DetalleInv() {
    }

    public DetalleInv(int idDetalle) {
        this.idDetalle = idDetalle;
    }

    public DetalleInv(int idDetalle, String descripcion, int cantidad, int precio) {
        this.idDetalle = idDetalle;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(int idDetalle) {
        this.idDetalle = idDetalle;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @XmlTransient
    public List<Revision> getRevisionList() {
        return revisionList;
    }

    public void setRevisionList(List<Revision> revisionList) {
        this.revisionList = revisionList;
    }

    public InvDepa getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(InvDepa idInventario) {
        this.idInventario = idInventario;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idDetalle != null ? idDetalle.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof DetalleInv)) {
//            return false;
//        }
//        DetalleInv other = (DetalleInv) object;
//        if ((this.idDetalle == null && other.idDetalle != null) || (this.idDetalle != null && !this.idDetalle.equals(other.idDetalle))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.DetalleInv[ idDetalle=" + idDetalle + " ]";
    }
    
}
