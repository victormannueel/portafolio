/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "INV_DEPA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InvDepa.findAll", query = "SELECT i FROM InvDepa i")
    , @NamedQuery(name = "InvDepa.findByIdInventario", query = "SELECT i FROM InvDepa i WHERE i.idInventario = :idInventario")
    , @NamedQuery(name = "InvDepa.findByFechaInventario", query = "SELECT i FROM InvDepa i WHERE i.fechaInventario = :fechaInventario")})
public class InvDepa implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_INVENTARIO")
    private int idInventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INVENTARIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInventario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInventario")
    private List<DetalleInv> detalleInvList;
    @JoinColumn(name = "ID_DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")
    @ManyToOne(optional = false)
    private Departamento idDepartamento;

    public InvDepa() {
    }

    public InvDepa(int idInventario) {
        this.idInventario = idInventario;
    }

    public InvDepa(int idInventario, Date fechaInventario) {
        this.idInventario = idInventario;
        this.fechaInventario = fechaInventario;
    }

    public int getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(int idInventario) {
        this.idInventario = idInventario;
    }

    public Date getFechaInventario() {
        return fechaInventario;
    }

    public void setFechaInventario(Date fechaInventario) {
        this.fechaInventario = fechaInventario;
    }

    @XmlTransient
    public List<DetalleInv> getDetalleInvList() {
        return detalleInvList;
    }

    public void setDetalleInvList(List<DetalleInv> detalleInvList) {
        this.detalleInvList = detalleInvList;
    }

    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Departamento idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idInventario != null ? idInventario.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof InvDepa)) {
//            return false;
//        }
//        InvDepa other = (InvDepa) object;
//        if ((this.idInventario == null && other.idInventario != null) || (this.idInventario != null && !this.idInventario.equals(other.idInventario))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.InvDepa[ idInventario=" + idInventario + " ]";
    }
    
}
