/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "ESTADO_USU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoUsu.findAll", query = "SELECT e FROM EstadoUsu e")
    , @NamedQuery(name = "EstadoUsu.findByIdEstadoUsu", query = "SELECT e FROM EstadoUsu e WHERE e.idEstadoUsu = :idEstadoUsu")
    , @NamedQuery(name = "EstadoUsu.findByDescripcion", query = "SELECT e FROM EstadoUsu e WHERE e.descripcion = :descripcion")})
public class EstadoUsu implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ESTADO_USU")
    private int idEstadoUsu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstadoUsu")
    private List<Sesion> sesionList;

    public EstadoUsu() {
    }

    public EstadoUsu(int idEstadoUsu) {
        this.idEstadoUsu = idEstadoUsu;
    }

    public EstadoUsu(int idEstadoUsu, String descripcion) {
        this.idEstadoUsu = idEstadoUsu;
        this.descripcion = descripcion;
    }

    public int getIdEstadoUsu() {
        return idEstadoUsu;
    }

    public void setIdEstadoUsu(int idEstadoUsu) {
        this.idEstadoUsu = idEstadoUsu;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Sesion> getSesionList() {
        return sesionList;
    }

    public void setSesionList(List<Sesion> sesionList) {
        this.sesionList = sesionList;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idEstadoUsu != null ? idEstadoUsu.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof EstadoUsu)) {
//            return false;
//        }
//        EstadoUsu other = (EstadoUsu) object;
//        if ((this.idEstadoUsu == null && other.idEstadoUsu != null) || (this.idEstadoUsu != null && !this.idEstadoUsu.equals(other.idEstadoUsu))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.EstadoUsu[ idEstadoUsu=" + idEstadoUsu + " ]";
    }
    
}
