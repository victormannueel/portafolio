/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "RESERVA_ACOM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReservaAcom.findAll", query = "SELECT r FROM ReservaAcom r")
    , @NamedQuery(name = "ReservaAcom.findByIdReservaAcompanante", query = "SELECT r FROM ReservaAcom r WHERE r.idReservaAcompanante = :idReservaAcompanante")})
public class ReservaAcom implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_RESERVA_ACOMPANANTE")
    private int idReservaAcompanante;
    @JoinColumn(name = "RUT_ACOMPANANTE", referencedColumnName = "RUT_ACOMPANANTE")
    @ManyToOne(optional = false)
    private Acompanante rutAcompanante;
    @JoinColumn(name = "ID_RESERVA", referencedColumnName = "ID_RESERVA")
    @ManyToOne(optional = false)
    private Reserva idReserva;

    public ReservaAcom() {
    }

    public ReservaAcom(int idReservaAcompanante) {
        this.idReservaAcompanante = idReservaAcompanante;
    }

    public int getIdReservaAcompanante() {
        return idReservaAcompanante;
    }

    public void setIdReservaAcompanante(int idReservaAcompanante) {
        this.idReservaAcompanante = idReservaAcompanante;
    }

    public Acompanante getRutAcompanante() {
        return rutAcompanante;
    }

    public void setRutAcompanante(Acompanante rutAcompanante) {
        this.rutAcompanante = rutAcompanante;
    }

    public Reserva getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Reserva idReserva) {
        this.idReserva = idReserva;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idReservaAcompanante != null ? idReservaAcompanante.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ReservaAcom)) {
//            return false;
//        }
//        ReservaAcom other = (ReservaAcom) object;
//        if ((this.idReservaAcompanante == null && other.idReservaAcompanante != null) || (this.idReservaAcompanante != null && !this.idReservaAcompanante.equals(other.idReservaAcompanante))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.ReservaAcom[ idReservaAcompanante=" + idReservaAcompanante + " ]";
    }
    
}
