/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "REVISION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Revision.findAll", query = "SELECT r FROM Revision r")
    , @NamedQuery(name = "Revision.findByIdRevision", query = "SELECT r FROM Revision r WHERE r.idRevision = :idRevision")
    , @NamedQuery(name = "Revision.findByEstado", query = "SELECT r FROM Revision r WHERE r.estado = :estado")})
public class Revision implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_REVISION")
    private int idRevision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private Character estado;
    @JoinColumn(name = "ID_CHECK_OUT", referencedColumnName = "ID_CHECK_OUT")
    @ManyToOne(optional = false)
    private CheckOut idCheckOut;
    @JoinColumn(name = "ID_DETALLE", referencedColumnName = "ID_DETALLE")
    @ManyToOne(optional = false)
    private DetalleInv idDetalle;

    public Revision() {
    }

    public Revision(int idRevision) {
        this.idRevision = idRevision;
    }

    public Revision(int idRevision, Character estado) {
        this.idRevision = idRevision;
        this.estado = estado;
    }

    public int getIdRevision() {
        return idRevision;
    }

    public void setIdRevision(int idRevision) {
        this.idRevision = idRevision;
    }

    public Character getEstado() {
        return estado;
    }

    public void setEstado(Character estado) {
        this.estado = estado;
    }

    public CheckOut getIdCheckOut() {
        return idCheckOut;
    }

    public void setIdCheckOut(CheckOut idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    public DetalleInv getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(DetalleInv idDetalle) {
        this.idDetalle = idDetalle;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idRevision != null ? idRevision.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Revision)) {
//            return false;
//        }
//        Revision other = (Revision) object;
//        if ((this.idRevision == null && other.idRevision != null) || (this.idRevision != null && !this.idRevision.equals(other.idRevision))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.Revision[ idRevision=" + idRevision + " ]";
    }
    
}
