/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "RESERVA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r")
    , @NamedQuery(name = "Reserva.findByIdReserva", query = "SELECT r FROM Reserva r WHERE r.idReserva = :idReserva")
    , @NamedQuery(name = "Reserva.findByFechaIngreso", query = "SELECT r FROM Reserva r WHERE r.fechaIngreso = :fechaIngreso")
    , @NamedQuery(name = "Reserva.findByFechaSalida", query = "SELECT r FROM Reserva r WHERE r.fechaSalida = :fechaSalida")
    , @NamedQuery(name = "Reserva.findByNumAcompanantes", query = "SELECT r FROM Reserva r WHERE r.numAcompanantes = :numAcompanantes")})
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_RESERVA")
    private int idReserva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_SALIDA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSalida;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_ACOMPANANTES")
    private int numAcompanantes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReserva")
    private List<PrecioFinal> precioFinalList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReserva")
    private List<ReservaAcom> reservaAcomList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReserva")
    private List<CheckIn> checkInList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReserva")
    private List<ReservaDepa> reservaDepaList;
    @JoinColumn(name = "ID_ABONO", referencedColumnName = "ID_ABONO")
    @ManyToOne(optional = false)
    private Abono idAbono;
    @JoinColumn(name = "ID_DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")
    @ManyToOne(optional = false)
    private Departamento idDepartamento;
    @JoinColumn(name = "ID_ESTADO_RESERVA", referencedColumnName = "ID_ESTADO_RESERVA")
    @ManyToOne(optional = false)
    private EstadoReserva idEstadoReserva;
    @JoinColumn(name = "ID_SERVICIO_EXTRA", referencedColumnName = "ID_SERVICIO_EXTRA")
    @ManyToOne(optional = false)
    private ServicioExtra idServicioExtra;
    @JoinColumn(name = "RUT_USUARIO", referencedColumnName = "RUT_USUARIO")
    @ManyToOne(optional = false)
    private Usuario rutUsuario;

    public Reserva() {
    }

     public Reserva(Date fechaIngreso, Date fechaSalida, int numAcompanantes, Abono idAbono, Departamento idDepartamento, EstadoReserva idEstadoReserva, ServicioExtra idServicioExtra, Usuario rutUsuario) {
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.numAcompanantes = numAcompanantes;
        this.idAbono = idAbono;
        this.idDepartamento = idDepartamento;
        this.idEstadoReserva = idEstadoReserva;
        this.idServicioExtra = idServicioExtra;
        this.rutUsuario = rutUsuario;
    }
    
    

     
    public Reserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public Reserva(int idReserva, Date fechaIngreso, Date fechaSalida, int numAcompanantes) {
        this.idReserva = idReserva;
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.numAcompanantes = numAcompanantes;
    }

    public Reserva(Date fechaIngreso, Date fechaSalida, int numAcompanantes) {
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.numAcompanantes = numAcompanantes;
    }

  
    
    public int getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public int getNumAcompanantes() {
        return numAcompanantes;
    }

    public void setNumAcompanantes(int numAcompanantes) {
        this.numAcompanantes = numAcompanantes;
    }

    @XmlTransient
    public List<PrecioFinal> getPrecioFinalList() {
        return precioFinalList;
    }

    public void setPrecioFinalList(List<PrecioFinal> precioFinalList) {
        this.precioFinalList = precioFinalList;
    }

    @XmlTransient
    public List<ReservaAcom> getReservaAcomList() {
        return reservaAcomList;
    }

    public void setReservaAcomList(List<ReservaAcom> reservaAcomList) {
        this.reservaAcomList = reservaAcomList;
    }

    @XmlTransient
    public List<CheckIn> getCheckInList() {
        return checkInList;
    }

    public void setCheckInList(List<CheckIn> checkInList) {
        this.checkInList = checkInList;
    }

    @XmlTransient
    public List<ReservaDepa> getReservaDepaList() {
        return reservaDepaList;
    }

    public void setReservaDepaList(List<ReservaDepa> reservaDepaList) {
        this.reservaDepaList = reservaDepaList;
    }

    public Abono getIdAbono() {
        return idAbono;
    }

    public void setIdAbono(Abono idAbono) {
        this.idAbono = idAbono;
    }

    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Departamento idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public EstadoReserva getIdEstadoReserva() {
        return idEstadoReserva;
    }

    public void setIdEstadoReserva(EstadoReserva idEstadoReserva) {
        this.idEstadoReserva = idEstadoReserva;
    }

    public ServicioExtra getIdServicioExtra() {
        return idServicioExtra;
    }

    public void setIdServicioExtra(ServicioExtra idServicioExtra) {
        this.idServicioExtra = idServicioExtra;
    }

    public Usuario getRutUsuario() {
        return rutUsuario;
    }

    public void setRutUsuario(Usuario rutUsuario) {
        this.rutUsuario = rutUsuario;
    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idReserva != null ? idReserva.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Reserva)) {
//            return false;
//        }
//        Reserva other = (Reserva) object;
//        if ((this.idReserva == null && other.idReserva != null) || (this.idReserva != null && !this.idReserva.equals(other.idReserva))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.Reserva[ idReserva=" + idReserva + " ]";
    }

}
