/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "SERV_DEPA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServDepa.findAll", query = "SELECT s FROM ServDepa s")
    , @NamedQuery(name = "ServDepa.findByIdServicioDepartamento", query = "SELECT s FROM ServDepa s WHERE s.idServicioDepartamento = :idServicioDepartamento")})
public class ServDepa implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SERVICIO_DEPARTAMENTO")
    private int idServicioDepartamento;
    @JoinColumn(name = "ID_DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")
    @ManyToOne(optional = false)
    private Departamento idDepartamento;
    @JoinColumn(name = "ID_SERVICIO", referencedColumnName = "ID_SERVICIO")
    @ManyToOne(optional = false)
    private Servicio idServicio;

    public ServDepa() {
    }

    public ServDepa(int idServicioDepartamento) {
        this.idServicioDepartamento = idServicioDepartamento;
    }

    public int getIdServicioDepartamento() {
        return idServicioDepartamento;
    }

    public void setIdServicioDepartamento(int idServicioDepartamento) {
        this.idServicioDepartamento = idServicioDepartamento;
    }

    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Departamento idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Servicio getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Servicio idServicio) {
        this.idServicio = idServicio;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idServicioDepartamento != null ? idServicioDepartamento.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ServDepa)) {
//            return false;
//        }
//        ServDepa other = (ServDepa) object;
//        if ((this.idServicioDepartamento == null && other.idServicioDepartamento != null) || (this.idServicioDepartamento != null && !this.idServicioDepartamento.equals(other.idServicioDepartamento))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.ServDepa[ idServicioDepartamento=" + idServicioDepartamento + " ]";
    }
    
}
