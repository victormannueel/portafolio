/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "SESION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sesion.findAll", query = "SELECT s FROM Sesion s")
    , @NamedQuery(name = "Sesion.findByIdSesion", query = "SELECT s FROM Sesion s WHERE s.idSesion = :idSesion")
    , @NamedQuery(name = "Sesion.findByUsuario", query = "SELECT s FROM Sesion s WHERE s.usuario = :usuario")
    , @NamedQuery(name = "Sesion.findByPass", query = "SELECT s FROM Sesion s WHERE s.pass = :pass")})
public class Sesion implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SESION")
    private int idSesion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "USUARIO")
    private String usuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "PASS")
    private String pass;
    @JoinColumn(name = "ID_TIPO_CARGO", referencedColumnName = "ID_TIPO_CARGO")
    @ManyToOne(optional = false)
    private Cargo idTipoCargo;
    @JoinColumn(name = "ID_ESTADO_USU", referencedColumnName = "ID_ESTADO_USU")
    @ManyToOne(optional = false)
    private EstadoUsu idEstadoUsu;
    @JoinColumn(name = "RUT_USUARIO", referencedColumnName = "RUT_USUARIO")
    @ManyToOne(optional = false)
    private Usuario rutUsuario;

    public Sesion() {
    }

    public Sesion(int idSesion) {
        this.idSesion = idSesion;
    }

    public Sesion(int idSesion, String usuario, String pass) {
        this.idSesion = idSesion;
        this.usuario = usuario;
        this.pass = pass;
    }

    public Sesion(String usuario, String pass, Cargo idTipoCargo, EstadoUsu idEstadoUsu, Usuario rutUsuario) {
        this.usuario = usuario;
        this.pass = pass;
        this.idTipoCargo = idTipoCargo;
        this.idEstadoUsu = idEstadoUsu;
        this.rutUsuario = rutUsuario;
    }
    

    public int getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(int idSesion) {
        this.idSesion = idSesion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Cargo getIdTipoCargo() {
        return idTipoCargo;
    }

    public void setIdTipoCargo(Cargo idTipoCargo) {
        this.idTipoCargo = idTipoCargo;
    }

    public EstadoUsu getIdEstadoUsu() {
        return idEstadoUsu;
    }

    public void setIdEstadoUsu(EstadoUsu idEstadoUsu) {
        this.idEstadoUsu = idEstadoUsu;
    }

    public Usuario getRutUsuario() {
        return rutUsuario;
    }

    public void setRutUsuario(Usuario rutUsuario) {
        this.rutUsuario = rutUsuario;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idSesion != null ? idSesion.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Sesion)) {
//            return false;
//        }
//        Sesion other = (Sesion) object;
//        if ((this.idSesion == null && other.idSesion != null) || (this.idSesion != null && !this.idSesion.equals(other.idSesion))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.Sesion[ idSesion=" + idSesion + " ]";
    }
    
}
