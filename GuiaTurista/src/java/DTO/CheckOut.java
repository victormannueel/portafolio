/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "CHECK_OUT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CheckOut.findAll", query = "SELECT c FROM CheckOut c")
    , @NamedQuery(name = "CheckOut.findByIdCheckOut", query = "SELECT c FROM CheckOut c WHERE c.idCheckOut = :idCheckOut")
    , @NamedQuery(name = "CheckOut.findByMulta", query = "SELECT c FROM CheckOut c WHERE c.multa = :multa")
    , @NamedQuery(name = "CheckOut.findByCostoReparacion", query = "SELECT c FROM CheckOut c WHERE c.costoReparacion = :costoReparacion")})
public class CheckOut implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CHECK_OUT")
    private int idCheckOut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MULTA")
    private int multa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COSTO_REPARACION")
    private int costoReparacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCheckOut")
    private List<Revision> revisionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCheckOut")
    private List<PrecioFinal> precioFinalList;
    @JoinColumn(name = "ID_CHECK_IN", referencedColumnName = "ID_CHECK_IN")
    @ManyToOne(optional = false)
    private CheckIn idCheckIn;
    @JoinColumn(name = "ID_ESTADO_CHECK", referencedColumnName = "ID_ESTADO_CHECK")
    @ManyToOne(optional = false)
    private EstadoCheck idEstadoCheck;

    public CheckOut() {
    }

    public CheckOut(int idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    public CheckOut(int idCheckOut, int multa, int costoReparacion) {
        this.idCheckOut = idCheckOut;
        this.multa = multa;
        this.costoReparacion = costoReparacion;
    }

    public int getIdCheckOut() {
        return idCheckOut;
    }

    public CheckOut(int multa, int costoReparacion) {
        this.multa = multa;
        this.costoReparacion = costoReparacion;
    }

    
    public void setIdCheckOut(int idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    public int getMulta() {
        return multa;
    }

    public void setMulta(int multa) {
        this.multa = multa;
    }

    public int getCostoReparacion() {
        return costoReparacion;
    }

    public void setCostoReparacion(int costoReparacion) {
        this.costoReparacion = costoReparacion;
    }

    @XmlTransient
    public List<Revision> getRevisionList() {
        return revisionList;
    }

    public void setRevisionList(List<Revision> revisionList) {
        this.revisionList = revisionList;
    }

    @XmlTransient
    public List<PrecioFinal> getPrecioFinalList() {
        return precioFinalList;
    }

    public void setPrecioFinalList(List<PrecioFinal> precioFinalList) {
        this.precioFinalList = precioFinalList;
    }

    public CheckIn getIdCheckIn() {
        return idCheckIn;
    }

    public void setIdCheckIn(CheckIn idCheckIn) {
        this.idCheckIn = idCheckIn;
    }

    public EstadoCheck getIdEstadoCheck() {
        return idEstadoCheck;
    }

    public void setIdEstadoCheck(EstadoCheck idEstadoCheck) {
        this.idEstadoCheck = idEstadoCheck;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idCheckOut != null ? idCheckOut.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof CheckOut)) {
//            return false;
//        }
//        CheckOut other = (CheckOut) object;
//        if ((this.idCheckOut == null && other.idCheckOut != null) || (this.idCheckOut != null && !this.idCheckOut.equals(other.idCheckOut))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.CheckOut[ idCheckOut=" + idCheckOut + " ]";
    }
    
}
