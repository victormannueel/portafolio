/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "ACOMPANANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acompanante.findAll", query = "SELECT a FROM Acompanante a")
    , @NamedQuery(name = "Acompanante.findByRutAcompanante", query = "SELECT a FROM Acompanante a WHERE a.rutAcompanante = :rutAcompanante")
    , @NamedQuery(name = "Acompanante.findByNombre", query = "SELECT a FROM Acompanante a WHERE a.nombre = :nombre")
    , @NamedQuery(name = "Acompanante.findByApellido", query = "SELECT a FROM Acompanante a WHERE a.apellido = :apellido")
    , @NamedQuery(name = "Acompanante.findByFechaNacimiento", query = "SELECT a FROM Acompanante a WHERE a.fechaNacimiento = :fechaNacimiento")})
public class Acompanante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "RUT_ACOMPANANTE")
    private String rutAcompanante;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Size(max = 100)
    @Column(name = "APELLIDO")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rutAcompanante")
    private List<ReservaAcom> reservaAcomList;

    public Acompanante() {
    }

    public Acompanante(String rutAcompanante) {
        this.rutAcompanante = rutAcompanante;
    }

    public Acompanante(String rutAcompanante, String nombre, Date fechaNacimiento) {
        this.rutAcompanante = rutAcompanante;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getRutAcompanante() {
        return rutAcompanante;
    }

    public void setRutAcompanante(String rutAcompanante) {
        this.rutAcompanante = rutAcompanante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @XmlTransient
    public List<ReservaAcom> getReservaAcomList() {
        return reservaAcomList;
    }

    public void setReservaAcomList(List<ReservaAcom> reservaAcomList) {
        this.reservaAcomList = reservaAcomList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rutAcompanante != null ? rutAcompanante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acompanante)) {
            return false;
        }
        Acompanante other = (Acompanante) object;
        if ((this.rutAcompanante == null && other.rutAcompanante != null) || (this.rutAcompanante != null && !this.rutAcompanante.equals(other.rutAcompanante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.Acompanante[ rutAcompanante=" + rutAcompanante + " ]";
    }
    
}
