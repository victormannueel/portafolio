/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "PRECIO_FINAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PrecioFinal.findAll", query = "SELECT p FROM PrecioFinal p")
    , @NamedQuery(name = "PrecioFinal.findByIdPrecioFinal", query = "SELECT p FROM PrecioFinal p WHERE p.idPrecioFinal = :idPrecioFinal")
    , @NamedQuery(name = "PrecioFinal.findByPrecioFinal", query = "SELECT p FROM PrecioFinal p WHERE p.precioFinal = :precioFinal")})
public class PrecioFinal implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PRECIO_FINAL")
    private int idPrecioFinal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO_FINAL")
    private int precioFinal;
    @JoinColumn(name = "ID_CHECK_OUT", referencedColumnName = "ID_CHECK_OUT")
    @ManyToOne(optional = false)
    private CheckOut idCheckOut;
    @JoinColumn(name = "ID_ESTADO_PAGO", referencedColumnName = "ID_ESTADO_PAGO")
    @ManyToOne(optional = false)
    private EstadoPago idEstadoPago;
    @JoinColumn(name = "ID_RESERVA", referencedColumnName = "ID_RESERVA")
    @ManyToOne(optional = false)
    private Reserva idReserva;
    @JoinColumn(name = "ID_TIPO_PAGO", referencedColumnName = "ID_TIPO_PAGO")
    @ManyToOne(optional = false)
    private TipoPago idTipoPago;

    public PrecioFinal() {
    }

    public PrecioFinal(int idPrecioFinal) {
        this.idPrecioFinal = idPrecioFinal;
    }

    public PrecioFinal(int idPrecioFinal, int precioFinal) {
        this.idPrecioFinal = idPrecioFinal;
        this.precioFinal = precioFinal;
    }

    public int getIdPrecioFinal() {
        return idPrecioFinal;
    }

    public void setIdPrecioFinal(int idPrecioFinal) {
        this.idPrecioFinal = idPrecioFinal;
    }

    public int getPrecioFinal() {
        return precioFinal;
    }

    public void setPrecioFinal(int precioFinal) {
        this.precioFinal = precioFinal;
    }

    public CheckOut getIdCheckOut() {
        return idCheckOut;
    }

    public void setIdCheckOut(CheckOut idCheckOut) {
        this.idCheckOut = idCheckOut;
    }

    public EstadoPago getIdEstadoPago() {
        return idEstadoPago;
    }

    public void setIdEstadoPago(EstadoPago idEstadoPago) {
        this.idEstadoPago = idEstadoPago;
    }

    public Reserva getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(Reserva idReserva) {
        this.idReserva = idReserva;
    }

    public TipoPago getIdTipoPago() {
        return idTipoPago;
    }

    public void setIdTipoPago(TipoPago idTipoPago) {
        this.idTipoPago = idTipoPago;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idPrecioFinal != null ? idPrecioFinal.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof PrecioFinal)) {
//            return false;
//        }
//        PrecioFinal other = (PrecioFinal) object;
//        if ((this.idPrecioFinal == null && other.idPrecioFinal != null) || (this.idPrecioFinal != null && !this.idPrecioFinal.equals(other.idPrecioFinal))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.PrecioFinal[ idPrecioFinal=" + idPrecioFinal + " ]";
    }
    
}
