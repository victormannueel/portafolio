/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "TRANSFERENCIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transferencia.findAll", query = "SELECT t FROM Transferencia t")
    , @NamedQuery(name = "Transferencia.findByRut", query = "SELECT t FROM Transferencia t WHERE t.rut = :rut")
    , @NamedQuery(name = "Transferencia.findByNumeroCuenta", query = "SELECT t FROM Transferencia t WHERE t.numeroCuenta = :numeroCuenta")
    , @NamedQuery(name = "Transferencia.findByMonto", query = "SELECT t FROM Transferencia t WHERE t.monto = :monto")
    , @NamedQuery(name = "Transferencia.findById", query = "SELECT t FROM Transferencia t WHERE t.id = :id")})
public class Transferencia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RUT")
    private String rut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUMERO_CUENTA")
    private int numeroCuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MONTO")
    private int monto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private int id;
    @JoinColumn(name = "ID_ESTADOPAGO", referencedColumnName = "ID_ESTADO_PAGO")
    @ManyToOne(optional = false)
    private EstadoPago idEstadopago;

    public Transferencia() {
    }

    public Transferencia(int id) {
        this.id = id;
    }

    public Transferencia(int id, String rut, int numeroCuenta, int monto) {
        this.id = id;
        this.rut = rut;
        this.numeroCuenta = numeroCuenta;
        this.monto = monto;
    }

    public Transferencia(String rut, int numeroCuenta, int monto) {
        this.rut = rut;
        this.numeroCuenta = numeroCuenta;
        this.monto = monto;
    }

   

    
    
    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public EstadoPago getIdEstadopago() {
        return idEstadopago;
    }

    public void setIdEstadopago(EstadoPago idEstadopago) {
        this.idEstadopago = idEstadopago;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Transferencia)) {
//            return false;
//        }
//        Transferencia other = (Transferencia) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.Transferencia[ id=" + id + " ]";
    }
    
}
