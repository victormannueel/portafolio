/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "SERVICIO_EXTRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ServicioExtra.findAll", query = "SELECT s FROM ServicioExtra s")
    , @NamedQuery(name = "ServicioExtra.findByIdServicioExtra", query = "SELECT s FROM ServicioExtra s WHERE s.idServicioExtra = :idServicioExtra")
    , @NamedQuery(name = "ServicioExtra.findByDescripcion", query = "SELECT s FROM ServicioExtra s WHERE s.descripcion = :descripcion")})
public class ServicioExtra implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SERVICIO_EXTRA")
    private int idServicioExtra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idServicioExtra")
    private List<Reserva> reservaList;

    public ServicioExtra() {
    }

    public ServicioExtra(int idServicioExtra) {
        this.idServicioExtra = idServicioExtra;
    }

    public ServicioExtra(int idServicioExtra, String descripcion) {
        this.idServicioExtra = idServicioExtra;
        this.descripcion = descripcion;
    }

    public int getIdServicioExtra() {
        return idServicioExtra;
    }

    public void setIdServicioExtra(int idServicioExtra) {
        this.idServicioExtra = idServicioExtra;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Reserva> getReservaList() {
        return reservaList;
    }

    public void setReservaList(List<Reserva> reservaList) {
        this.reservaList = reservaList;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idServicioExtra != null ? idServicioExtra.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ServicioExtra)) {
//            return false;
//        }
//        ServicioExtra other = (ServicioExtra) object;
//        if ((this.idServicioExtra == null && other.idServicioExtra != null) || (this.idServicioExtra != null && !this.idServicioExtra.equals(other.idServicioExtra))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "DTO.ServicioExtra[ idServicioExtra=" + idServicioExtra + " ]";
    }
    
}
