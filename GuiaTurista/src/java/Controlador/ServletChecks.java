/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.CheckInFacade;
import DAO.CheckOutFacade;
import DAO.ReservaFacade;
import DTO.CheckIn;
import DTO.CheckOut;
import DTO.EstadoCheck;
import DTO.Reserva;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kevin
 */
public class ServletChecks extends HttpServlet {

    @EJB
    private CheckOutFacade checkOutFacade;

    @EJB
    private CheckInFacade checkInFacade;

    @EJB
    private ReservaFacade reservaFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        String opcion = request.getParameter("btnAccion");
        if (opcion.equals("Buscar")) {
            listarNumeroR(request, response);
        }
        if (opcion.equals("GuardarIN")) {
            guardarCheckIn(request, response);
        }
        if (opcion.equals("BuscarOUT")) {
            buscarCheckIN(request, response);
        }
        if (opcion.equals("GuardarOUT")) {
            guardarCheckOut(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServletChecks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServletChecks.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void listarNumeroR(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("txtId").trim());
            if (id == reservaFacade.recuperarIDReserva(id)) {
                request.getSession().setAttribute("totalRestante", reservaFacade.totalConAbono(id));
                request.getSession().setAttribute("listaNumeroR", reservaFacade.recuperarIDReserva(id));
                int idReserva = reservaFacade.recuperarIDReserva(id);
                request.getSession().setAttribute("idreserva", idReserva);
                request.getSession().setAttribute("rutAsociado", reservaFacade.recuperarRutAsociado(id));
                response.sendRedirect("Funcionario/ingreso.jsp");

            } else {
                request.getSession().setAttribute("error", "Reserva no existe");
                response.sendRedirect("Funcionario/ingreso.jsp");
            }
        } catch (IOException | NumberFormatException e) {
            request.getSession().setAttribute("error", "Error al ingresar los datos");
            response.sendRedirect("Funcionario/ingreso.jsp");
        }

    }

    private void guardarCheckIn(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        HttpSession session = request.getSession();
        String cliConfirmacion = request.getParameter("cbo");
        Character c = cliConfirmacion.charAt(0);
        int pagoTotalCkeckIn = (int) session.getAttribute("totalRestante");
        int idReserva = (int) session.getAttribute("idreserva");
        request.getSession().setAttribute("idreserva", idReserva);

        CheckIn chei = new CheckIn(c, pagoTotalCkeckIn);

        Reserva re = new Reserva(idReserva);
        try {
            if (c.equals("S")) {
                request.getSession().setAttribute("error", "No se pudo realizar ingreso");
                response.sendRedirect("Funcionario/ingreso.jsp");
            } else {
                if (checkInFacade.recuperarIDReservaCheck(idReserva) == false) {
                    if (checkInFacade.AgregarCheckIn(chei, re) == true) {
                        request.getSession().setAttribute("correcto", "Ingreso realizado");
                        response.sendRedirect("Funcionario/ingreso.jsp");
                    } else {
                        request.getSession().setAttribute("error", "Ya se encuentra registrado este ingreso");
                        response.sendRedirect("Funcionario/ingreso.jsp");
                    }
                } else {
                    request.getSession().setAttribute("error", "No se pudo realizar ingreso");
                    response.sendRedirect("Funcionario/ingreso.jsp");
                }
            }
        } catch (IOException e) {
            request.getSession().setAttribute("error", "No se pudo realizar ingreso");
            response.sendRedirect("Funcionario/ingreso.jsp");
        }

    }

    private void buscarCheckIN(HttpServletRequest request, HttpServletResponse response) throws IOException {

        int id = Integer.parseInt(request.getParameter("txtId").trim());

        try {
            if (checkInFacade.validarIngresoId(id) == true) {
                request.getSession().setAttribute("idCheck", checkInFacade.recuperarIdCheckin(id));
                int idReserva = reservaFacade.recuperarIDReserva(id);
                request.getSession().setAttribute("idreserva", idReserva);
                request.getSession().setAttribute("rutAsociado", reservaFacade.recuperarRutAsociado(id));
                response.sendRedirect("Funcionario/salida.jsp");

            } else {

                request.getSession().setAttribute("error", "Reserva no existe");
                response.sendRedirect("Funcionario/salida.jsp");

            }
        } catch (IOException | NumberFormatException e) {
            request.getSession().setAttribute("error", "Error al ingresar los datos");
            response.sendRedirect("Funcionario/salida.jsp");
        }
    }

    private void guardarCheckOut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {

        int multa = Integer.parseInt(request.getParameter("txtMulta"));
        int costoReparacion = Integer.parseInt(request.getParameter("txtCostoReparacion"));

        HttpSession session = request.getSession();
        int id = (int) session.getAttribute("idreserva");
        int totalSinabono = reservaFacade.totalSinAbono(id);
        int sumaTotal = multa + costoReparacion+totalSinabono;
        int pagarAhora = multa + costoReparacion;
        String cliConfirmacion = request.getParameter("cbo");

        int idcheck = (int) session.getAttribute("idCheck");
        request.getSession().setAttribute("idreserva", idcheck);

        CheckOut cheickout = new CheckOut(multa, costoReparacion);

        EstadoCheck estadoCheck = new EstadoCheck(Integer.parseInt(cliConfirmacion));

        CheckIn chei = new CheckIn(idcheck);

        try {
            if (checkOutFacade.AgregarCheckOut(cheickout, estadoCheck, chei)) {
                request.setAttribute("correcto", "ingreso registrado");
                request.getRequestDispatcher("Funcionario/final.jsp").forward(request, response);
            } else {
                request.setAttribute("error", "ingreso no registrado");
                request.getRequestDispatcher("Funcionario/salida.jsp").forward(request, response);
            }
        } catch (Exception ex) {
            request.setAttribute("error", "Salida no resgitrada");
            request.getRequestDispatcher("Funcionario/salida.jsp").forward(request, response);
        }
    }
}
