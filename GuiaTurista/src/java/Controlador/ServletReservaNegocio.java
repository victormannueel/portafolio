/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.ReservaFacade;
import DTO.Abono;
import DTO.Departamento;
import DTO.EstadoReserva;
import DTO.Reserva;
import DTO.ServicioExtra;
import DTO.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kevin
 */
public class ServletReservaNegocio extends HttpServlet {

    @EJB
    private ReservaFacade reservaFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.text.ParseException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        try {

            String fechaI = request.getParameter("txtFechaInicio");
            
            
            Date fechaIngreso = new SimpleDateFormat("yyyy-MM-dd").parse(fechaI);
            
            String fechaS = request.getParameter("txtFechaSalida");
            Date fechaSalida = new SimpleDateFormat("yyyy-MM-dd").parse(fechaS);
 
            
            int numero = Integer.parseInt(request.getParameter("txtNumero"));
            String servicioExtra = request.getParameter("checkbox");
            HttpSession session = request.getSession();
            String rut = (String) session.getAttribute("idRut");
            int estadoReserva = Integer.parseInt(request.getParameter("txtEstadoReserva"));
            int depa = Integer.parseInt(request.getParameter("idDepa"));

            ServicioExtra se = new ServicioExtra(Integer.parseInt(servicioExtra));
            Usuario u = new Usuario(rut);
            EstadoReserva e = new EstadoReserva(estadoReserva);
            int a = 1;
            Abono abono = new Abono(a);

            Departamento d = new Departamento(depa);
            Reserva r = new Reserva(fechaIngreso, fechaSalida, numero);
            if (reservaFacade.agregarReserva(r, se, u, e, abono, d) == true) {
                request.getSession().setAttribute("numerop", reservaFacade.recuperarIDReserva(a));
                request.setAttribute("msj", "Reserva en proceso");
                request.getRequestDispatcher("negocioFinal.jsp").forward(request, response);
            //} else {
                //request.setAttribute("msj", "Reserva en proceso");
              //  request.getRequestDispatcher("negocioFinal.jsp").forward(request, response);
            }
        } catch (IOException | NumberFormatException | SQLException | ServletException e) {
            request.setAttribute("err", "Reintente");
            request.getRequestDispatcher("negocioReserva.jsp").forward(request, response);
        }

    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ParseException ex) {
            Logger.getLogger(ServletReservaNegocio.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);

        } catch (ParseException ex) {
            Logger.getLogger(ServletReservaNegocio.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
