/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.SesionFacade;
import DAO.UsuarioFacade;
import DTO.Cargo;
import DTO.EstadoUsu;
import DTO.Sesion;
import DTO.Usuario;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kevin
 */
public class ServletUsuario extends HttpServlet {

    @EJB
    private UsuarioFacade usuarioFacade;

    @EJB
    private SesionFacade sesionFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String opcion = request.getParameter("btnAccion");
        if (opcion.equals("Registrar")) {
            registrar(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void registrar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {

            String rut = request.getParameter("txtRut");
            String nombre = request.getParameter("txtNombre");
            String apellido = request.getParameter("txtApellido");
            String email = request.getParameter("txtEmail");
            int numero = Integer.parseInt(request.getParameter("txtNumero"));

            String fechaNacimiento = request.getParameter("txtFecha");

            Date fechaIngreso = new SimpleDateFormat("yyyy-MM-dd").parse(fechaNacimiento);

            Usuario u = new Usuario(rut, nombre, apellido, email, numero, fechaIngreso);
            if (usuarioFacade.registrarUsuario(u) == true) {
                String user = request.getParameter("txtUser");
                String pass = request.getParameter("txtPass");
                EstadoUsu eu = new EstadoUsu(3);
                Cargo c = new Cargo(3);
                Sesion s = new Sesion(user, pass, c, eu, u);
                if (sesionFacade.resgistrarSesion(s, u, c, eu) == true) {
                    response.sendRedirect("MantenedorUsuario/redireccionar.jsp");
                    request.getSession().setAttribute("exito", "Tu cuenta se a creado con exito!");
                } else {
                    response.sendRedirect("MantenedorUsuario/registrarUsuario.jsp");
                    request.getSession().setAttribute("errorUsu", "Estamos solucionado los problemas");
                }
            } else {
                response.sendRedirect("MantenedorUsuario/registrarUsuario.jsp");
                request.getSession().setAttribute("errorUsu", "Error al ingresar los datos");
            }
        } catch (Exception e) {
            response.sendRedirect("MantenedorUsuario/registrarUsuario.jsp");
            request.getSession().setAttribute("errorUsu", "Error al ingresar los datos");
        }

    }

}
