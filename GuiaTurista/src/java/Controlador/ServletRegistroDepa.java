/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.DepartamentoFacade;
import DTO.Ciudad;
import DTO.Comuna;
import DTO.Departamento;
import DTO.Disponibilidad;
import DTO.Estado;
import DTO.Region;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Kevin
 */
@MultipartConfig
public class ServletRegistroDepa extends HttpServlet {

    @EJB
    private DepartamentoFacade departamentoFacade;

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try {
            String nombre = request.getParameter("txtNombre");
            String descripcion = request.getParameter("txtDescripcion");
            int numeroHabitacion = Integer.parseInt(request.getParameter("txtHabitacion"));
            int precioNoche = Integer.parseInt(request.getParameter("txtPrecio"));
            Part archivo = request.getPart("archivo");
            String imagen = String.valueOf(archivo.getSubmittedFileName());

            int cantHabitaciones = Integer.parseInt(request.getParameter("txtCantidadHabitaciones"));
            int region = Integer.parseInt(request.getParameter("txtRegion"));
            int ciudad = Integer.parseInt(request.getParameter("txtCiudad"));
            int comuna = Integer.parseInt(request.getParameter("txtComuna"));
            int estado = Integer.parseInt(request.getParameter("txtEstado"));
            int disponibilidad = Integer.parseInt(request.getParameter("txtDisponibilidad"));
            String direccion = request.getParameter("txtDireccion");
            Region r = new Region(region);
            Ciudad c = new Ciudad(ciudad);
            Comuna co = new Comuna(comuna);
            Estado es = new Estado(estado);
            Disponibilidad dis = new Disponibilidad(disponibilidad);

            Departamento d = new Departamento(nombre, descripcion, numeroHabitacion, precioNoche, imagen, cantHabitaciones, direccion, c, co, dis, es, r);

            File carpetDestino = new File("C:\\Users\\Oscar\\Documents\\NetBeansProjects\\GuiaTurista\\web\\images");
            File ArchivoDestino = new File(carpetDestino, archivo.getSubmittedFileName());
            InputStream stream = archivo.getInputStream();
            departamentoFacade.create(d);
            Files.copy(stream, ArchivoDestino.toPath(), StandardCopyOption.REPLACE_EXISTING);
            response.sendRedirect("registroDepartamento.jsp");
        } catch (Exception e) {
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
