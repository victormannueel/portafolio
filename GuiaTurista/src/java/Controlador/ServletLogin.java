/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import servicioweb.Login;
import servicioweb.Login_Service;

/**
 *
 * @author Kevin
 */
public class ServletLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String opcion = request.getParameter("btnAccion");
        if (opcion.equals("Ingresar")) {
            login(request, response);
        }
        if (opcion.equals("Registrar")) {
            registrar(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            HttpSession session = request.getSession();
            Login_Service services = new Login_Service();
            Login sesLogin = services.getLoginPort();
            String user = request.getParameter("txtUsuario").trim();
            String pass = request.getParameter("txtPass").trim();
            int tipoUsuario = Integer.parseInt(request.getParameter("tipoUsu"));

            int id = sesLogin.recuperarID(user, pass);
            int tipoUsuC = sesLogin.recuperarTipo(user, pass);
            int tipoUsu = sesLogin.recuperarTipoUsu(user, pass);
            String rut = sesLogin.recuperarRut(user, pass);
            session.setAttribute("usuarioID", id);
            request.setAttribute("tisessionpo", tipoUsuC);
            request.setAttribute("user", user);
            session.setAttribute("idRut", rut);
            request.getSession().setAttribute("pass", pass);

            if (tipoUsuario == tipoUsuC) {
                switch (tipoUsu) {
                    case 2:
                        request.getRequestDispatcher("login.jsp").forward(request, response);
                        request.getSession().setAttribute("error", "Su cuenta se encuentra en estado de evaluacion");
                        break;
                    case 3:
                        request.getRequestDispatcher("login.jsp").forward(request, response);
                        request.getSession().setAttribute("error", "Tu cuenta aun no esta habilitada");
                        break;
                    case 1:
                        switch (tipoUsuC) {
                            case 3:
                                if(user.length()>0 && pass.length()>0){
                                request.getRequestDispatcher("menuPrincipal.jsp").forward(request, response);
                                request.getSession().setAttribute("user", user);
                                }
                                break;
                            case 2:
                                request.getRequestDispatcher("Funcionario/menuCheck.jsp").forward(request, response);
                                request.getSession().setAttribute("user", user);
                                break;
                            case 1:
                                request.getRequestDispatcher("login.jsp").forward(request, response);
                                request.getSession().setAttribute("error", "Este usuario no esta habilitado en esta plataforma");
                                break;
                            default:
                                request.getRequestDispatcher("login.jsp").forward(request, response);
                                request.getSession().setAttribute("error", "Este usuario no existe");
                                break;
                        }
                    default:
                        request.getRequestDispatcher("login.jsp").forward(request, response);
                        request.getSession().setAttribute("error", "Este usuario no existe");
                        break;
                }
            } else {
                request.getRequestDispatcher("login.jsp").forward(request, response);
                request.getSession().setAttribute("error", "Este usuario no corresponde al tipo de usuario");
            }
        } catch (Exception e) {
            response.sendRedirect("login.jsp");
            request.getSession().setAttribute("error", "Problemas con los servidores");
        }
    }

    private void registrar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("MantenedorUsuario/registrarUsuario.jsp");
    }

}
