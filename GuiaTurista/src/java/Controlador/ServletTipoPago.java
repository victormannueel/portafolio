/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.ReservaFacade;
import DAO.TarjetaFacade;
import DAO.TransferenciaFacade;
import DTO.EstadoPago;
import DTO.Tarjeta;
import DTO.Transferencia;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kevin
 */
public class ServletTipoPago extends HttpServlet {

    @EJB
    private TransferenciaFacade transferenciaFacade;

    @EJB
    private ReservaFacade reservaFacade;

    @EJB
    private TarjetaFacade tarjetaFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String opcion = request.getParameter("btnAccion");
        if (opcion.equals("Pagar")) {
            pagoTarjetaFinal(request, response);
        }
        if (opcion.equals("Transferencia")) {
            pagoTransferencia(request, response);
        }
        String cbo = request.getParameter("cbo");
        try {
            if (cbo.equals("0")) {
                request.getSession().setAttribute("error", "Debe elegir un metodo de pago");
                request.getRequestDispatcher("negocioFinal.jsp").forward(request, response);
            }
            if (cbo.equals("1")) {
                HttpSession session = request.getSession();
                String rut = (String) session.getAttribute("idRut");
                request.getRequestDispatcher("SalidaTarjeta.jsp").forward(request, response);
            }
            if (cbo.equals("2")) {
                HttpSession session = request.getSession();
                String rut = (String) session.getAttribute("idRut");
                request.getRequestDispatcher("transferencia.jsp").forward(request, response);
            }

        } catch (Exception e) {

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected void pagoTarjetaFinal(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String rut = (String) session.getAttribute("idRut");
        int id = (int) session.getAttribute("idReservaFinal");
        int numeroTarjeta = Integer.parseInt(request.getParameter("txtTarjeta"));

        int numeroSeguridad = Integer.parseInt(request.getParameter("txtNumeroSeguridad"));
        int total = reservaFacade.totalSinAbono(id);
        int totalConAbono = reservaFacade.totalConAbono(id);

        request.getSession().setAttribute("numeroR", id);
        request.getSession().setAttribute("total", total);
        tarjetaFacade.pagotarjeta(rut, numeroTarjeta, numeroSeguridad, 20000);
        request.getRequestDispatcher("redireccionFinal.jsp").forward(request, response);

    }

    private void pagoTransferencia(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            String rut = (String) session.getAttribute("idRut");
            int numero = Integer.parseInt(request.getParameter("txtNumeroCuenta"));
            int abono = 20000;
            int idEstado = 2;
            EstadoPago es = new EstadoPago(idEstado);
            Transferencia tra = new Transferencia(rut, numero, abono);
            transferenciaFacade.aggregarTransferencia(tra, es);
            request.getSession().setAttribute("correcto", "Tu pago a sido solicitado se encuentra en espera");
            request.getRequestDispatcher("TransferenciaFinal.jsp").forward(request, response);
        } catch (Exception e) {
        }

    }
}
