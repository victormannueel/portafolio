/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.CheckIn;
import DTO.Reserva;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kevin
 */
@Stateless
public class CheckInFacade extends AbstractFacade<CheckIn> {

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;
    private Connection conexion;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CheckInFacade() {
        super(CheckIn.class);
    }

    public boolean AgregarCheckIn(CheckIn chei, Reserva re) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{ call SP_INSERTAR_CHECKIN(?,?,?) }";
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setString(1, String.valueOf(chei.getCliConfirmacion()));
            cstmt.setInt(2, chei.getPagoReserva());
            cstmt.setInt(3, re.getIdReserva());
            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;
    }

    public boolean recuperarIDReservaCheck(int id) {
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM CHECK_IN WHERE ID_RESERVA = '" + id + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                resultados.getString(4);
                return true;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return false;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return false;
    }

    public int recuperarIdCheckin(int id) {
        int usuId = 0;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM CHECK_IN WHERE ID_RESERVA ='" + id + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                usuId = resultados.getInt(1);
                return usuId;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return usuId;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return usuId;
    }
    
     public boolean validarIngresoId(int id) {
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM CHECK_IN WHERE ID_RESERVA = '" + id + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                resultados.getString(1);
                return true;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return false;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return false;
    }
}
