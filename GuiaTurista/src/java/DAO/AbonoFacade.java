/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.Abono;
import DTO.TipoPago;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kevin
 */
@Stateless
public class AbonoFacade extends AbstractFacade<Abono> {

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;
    private Connection conexion;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AbonoFacade() {
        super(Abono.class);
    }

    public boolean agregarAbono(Abono a, TipoPago t) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{call SP_REGISTRAR_ABONO(?,?)}";
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setInt(1, a.getPrecio());
            cstmt.setObject(2, t.getIdTipoPago());
            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;

    }

}
