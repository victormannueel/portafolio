/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.Abono;
import DTO.Departamento;
import DTO.EstadoReserva;
import DTO.Reserva;
import DTO.ServicioExtra;
import DTO.Usuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.ws.WebServiceRef;
import servicioweb.Reserva_Service;

/**
 *
 * @author Kevin
 */
@Stateless
public class ReservaFacade extends AbstractFacade<Reserva> {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/Servicios/Reserva.wsdl")
    private Reserva_Service service;

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;
    private Connection conexion;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReservaFacade() {
        super(Reserva.class);
    }

    public java.util.List<servicioweb.Reserva_Type> mostrarReservaFinal(java.lang.String rut) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        servicioweb.Reserva port = service.getReservaPort();
        return port.mostrarReservaFinal(rut);
    }

    public boolean agregarReserva(Reserva reserva, ServicioExtra servicioExtra, Usuario rutUsuario, EstadoReserva estadoReserva, Abono abono, Departamento depa) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{call SP_REGISTRAR_RESERVA(?,?,?,?,?,?,?,?)}";
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setDate(1, new java.sql.Date(reserva.getFechaIngreso().getTime()));
            cstmt.setDate(2, new java.sql.Date(reserva.getFechaSalida().getTime()));
            cstmt.setInt(3, reserva.getNumAcompanantes());
            cstmt.setInt(4, servicioExtra.getIdServicioExtra());
            cstmt.setString(5, rutUsuario.getRutUsuario());
            cstmt.setInt(6, estadoReserva.getIdEstadoReserva());
            cstmt.setInt(7, abono.getIdAbono());
            cstmt.setInt(8, depa.getIdDepartamento());

            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;

    }

    public int recuperarIDReserva(int id) {
        int usuId = 0;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM RESERVA WHERE ID_RESERVA ='" + id + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                usuId = Integer.parseInt(resultados.getString(1));
                return usuId;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return usuId;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return usuId;

    }

    public int totalConAbono(int id) {
        int total = 0;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT (ROUND(FECHA_SALIDA-FECHA_INGRESO,0)*DEPARTAMENTO.PRECIO_NOCHE)-20000 AS TOTALCONABONO  FROM RESERVA JOIN DEPARTAMENTO ON RESERVA.ID_DEPARTAMENTO = DEPARTAMENTO.ID_DEPARTAMENTO  WHERE ID_RESERVA = '" + id + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                total = resultados.getInt("TOTALCONABONO");
                return total;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return 0;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return 0;
    }

    public int totalSinAbono(int id) {
        int total = 0;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT ROUND(RE.FECHA_SALIDA-RE.FECHA_INGRESO)*DEPA.PRECIO_NOCHE AS DIAS FROM RESERVA RE JOIN DEPARTAMENTO DEPA ON RE.ID_DEPARTAMENTO = DEPA.ID_DEPARTAMENTO WHERE RE.ID_RESERVA =   '" + id + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                total = resultados.getInt("DIAS");
                return total;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return 0;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return 0;
    }

    public String recuperarRutAsociado(int id) {
        String rut = null;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM RESERVA WHERE ID_RESERVA ='" + id + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                rut = resultados.getString(6);
                return rut;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return null;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return rut;
    }

//    public Object generarAbono(int id) {
//        Query query = em.createQuery("SELECT depa.idDepartamento.precioNoche*(r.fechaSalida - r.fechaIngreso) FROM Reserva r JOIN R.reservaDepaList depa  AND d.idDepartamento = " + id + "");
//        return query.getResultList();
//    }

    public int ultimoIdReserva() {
        int total = 0;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT MAX(RESER.ID_RESERVA) AS ID FROM RESERVA RESER";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                total = resultados.getInt("ID");
                return total;
            } else {
                String mensaje = "usuario y/o contraseña incorrectos";
                return 0;
            }
        } catch (Exception e) {

            e.getMessage();
        }
        return 0;
    }
}
