/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.Departamento;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.jws.WebParam;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.ws.WebServiceRef;
import oracle.jdbc.internal.OracleTypes;
import servicioweb.Reserva_Service;

/**
 *
 * @author Kevin
 */
@Stateless
public class DepartamentoFacade extends AbstractFacade<Departamento> {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/Servicios/Reserva.wsdl")
    private Reserva_Service service;

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;
    private Connection conexion;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DepartamentoFacade() {
        super(Departamento.class);
    }

    public Object ListarDepartamento() {
        Query query = em.createQuery("SELECT d FROM Departamento d JOIN d.idEstado estado JOIN d.idDisponibilidad dis WHERE estado.idEstado = 1 AND dis.idDisponibilidad = 2");
        return query.getResultList();
    }

    public Object BuscarDepa(int id) {
        Query query = em.createQuery("SELECT d FROM Departamento d JOIN d.idEstado estado WHERE estado.idEstado =1 AND d.idDepartamento = " + id + "");
        return query.getResultList();
    }

    public java.util.List<servicioweb.Departamento> mostrarDepaPorId(int id) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        servicioweb.Reserva port = service.getReservaPort();
        return port.mostrarDepaPorId(id);
    }
}
