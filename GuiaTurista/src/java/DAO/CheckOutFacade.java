/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.CheckIn;
import DTO.CheckOut;
import DTO.EstadoCheck;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kevin
 */
@Stateless
public class CheckOutFacade extends AbstractFacade<CheckOut> {

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;
    private Connection conexion;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CheckOutFacade() {
        super(CheckOut.class);
    }

    public boolean AgregarCheckOut(CheckOut checkOut, EstadoCheck eche, CheckIn chei) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{call SP_INSERTAR_CHECKOUT(?,?,?,?)}"; // 
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setInt(1, checkOut.getMulta());
            cstmt.setInt(2, checkOut.getCostoReparacion());
            cstmt.setInt(3, eche.getIdEstadoCheck());
            cstmt.setInt(4, chei.getIdCheckIn());

            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;
    }
}
