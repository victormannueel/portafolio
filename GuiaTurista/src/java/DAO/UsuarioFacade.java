/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.Usuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Kevin
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;
    private Connection conexion;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    public Boolean registrarUsuario(Usuario u) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{call SP_REGISTRAR_USUARIO(?,?,?,?,?,?)}";
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setString(1, u.getRutUsuario());
            cstmt.setString(2, u.getNombre());
            cstmt.setString(3, u.getApellido());
            cstmt.setString(4, u.getEmail());
            cstmt.setInt(5, u.getNumero());
            cstmt.setDate(6, new java.sql.Date(u.getFechaNacimiento().getTime()));
            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;
    }

    public Object mostrarUsuario(String rut) {
        Query query = em.createQuery("SELECT u FROM Usuario u WHERE u.rutUsuario = '" + rut + "'");
        return query.getResultList();
    }
}
