/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.ReservaDepa;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kevin
 */
@Stateless
public class ReservaDepaFacade extends AbstractFacade<ReservaDepa> {

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReservaDepaFacade() {
        super(ReservaDepa.class);
    }
    
}
