/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.EstadoPago;
import DTO.Transferencia;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kevin
 */
@Stateless
public class TransferenciaFacade extends AbstractFacade<Transferencia> {

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;
    private Connection conexion;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TransferenciaFacade() {
        super(Transferencia.class);
    }
     public boolean aggregarTransferencia(Transferencia tran, EstadoPago pago) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{call SP_REGISTRAR_TRANSFERENCIA(?,?,?,?)}";
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setString(1, tran.getRut());
            cstmt.setInt(2, tran.getNumeroCuenta());
            cstmt.setInt(3, tran.getMonto());
            cstmt.setInt(4, pago.getIdEstadoPago());
            
            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;

    }
}
