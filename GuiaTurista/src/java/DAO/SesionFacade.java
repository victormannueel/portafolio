/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexion.Conexion;
import DTO.Cargo;
import DTO.EstadoUsu;
import DTO.Sesion;
import DTO.Usuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kevin
 */
@Stateless
public class SesionFacade extends AbstractFacade<Sesion> {

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SesionFacade() {
        super(Sesion.class);
    }
    private Connection conexion;

    public Boolean resgistrarSesion(Sesion usu, Usuario rut, Cargo TipoCargo, EstadoUsu estadoUsu) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{call SP_REGISTRAR_SESION(?,?,?,?,?)}";
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setString(1, usu.getUsuario());
            cstmt.setString(2, usu.getPass());
            cstmt.setString(3, rut.getRutUsuario());
            cstmt.setInt(4, TipoCargo.getIdTipoCargo());
            cstmt.setInt(5, estadoUsu.getIdEstadoUsu());
            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;
    }
}
