/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.Tarjeta;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.WebServiceRef;
import servicioweb.Pago_Service;

/**
 *
 * @author Kevin
 */
@Stateless
public class TarjetaFacade extends AbstractFacade<Tarjeta> {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/Servicios/Pago.wsdl")
    private Pago_Service service;

    @PersistenceContext(unitName = "GuiaTuristaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TarjetaFacade() {
        super(Tarjeta.class);
    }

    public boolean pagotarjeta(java.lang.String rut, int numeroTarjeta, int numeroSeguridad, int precio) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        servicioweb.Pago port = service.getPagoPort();
        return port.pagotarjeta(rut, numeroTarjeta, numeroSeguridad, precio);
    }
    
}
