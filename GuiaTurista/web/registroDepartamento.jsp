
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <h1>Hello World!</h1>
        <form action="registroDepartamento" method="POST" enctype="multipart/form-data">
            <table border="1">
                <tbody>
                    <tr>
                        <td>Nombre</td>
                        <td><input type="text" required="" name="txtNombre"></td>
                    </tr>
                    <tr>
                        <td>Descripcion</td>
                        <td><input type="text" required="" name="txtDescripcion"</td>
                    </tr>
                    <tr>
                        <td>Numero habitacion</td>
                        <td><input type="number" required="" name="txtHabitacion"</td>
                    </tr>
                    <tr>
                        <td>Precio noche</td>
                        <td><input type="number" required="" name="txtPrecio"</td>
                    </tr>
                    <tr>
                        <td>Imagen</td>
                        <td><input type="file" required="" name="archivo"></td>
                    </tr>
                    <tr>
                        <td>Cant habitaciones</td>
                        <td><input type="number" required="" name="txtCantidadHabitaciones"</td>
                    </tr>
                    <tr>
                        <td>Id region</td>
                        <td><input type="number" required="" name="txtRegion"</td>
                    </tr>
                    <tr>
                        <td>Id ciudad</td>
                        <td><input type="number" required="" name="txtCiudad"</td>
                    </tr>
                    <tr>
                        <td>Id comuna</td>
                        <td><input type="number" required="" name="txtComuna"</td>
                    </tr>
                    <tr>
                        <td>Id Estado</td>
                        <td><input type="number" required="" name="txtEstado"</td>
                    </tr>
                    <tr>
                        <td>Id Disponibilidad</td>
                        <td><input type="number" required="" name="txtDisponibilidad"</td>
                    </tr>
                    <tr>
                        <td>Direccion</td>
                        <td><input type="text" required="" name="txtDireccion"</td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="btnAccion" value="Registrar"></td>
                    </tr>
                </tbody>
            </table>
        </form>
    </body>
</html>
