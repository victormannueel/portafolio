
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
        <meta http-equiv="Refresh" content="3;URL=menuFinal.jsp">
        <title>Redirecion</title>

    </head><body> 
        <c:if test="${msj!=null}">
            <script type="text/javascript" >
                swal({
                    title: "Mensaje",
                    text: "${msj}",
                    type: "success",
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Aceptar'
                });
            </script>
        </c:if>
        <c:if test="${err!=null}">
            <script type="text/javascript" >
                swal({
                    title: "Mensaje",
                    text: "${err}",
                    type: "warning",
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Aceptar'
                });
            </script>
        </c:if>
    <center>
        <div class="jumbotron">
            <h1>PAGANDO VIA TARJETA</h1>
        </div>
        <div>
            <img src="images/loading.gif" alt=""  width="100%" height="100%"/>
        </div>
    </center>
</body>
</html>
