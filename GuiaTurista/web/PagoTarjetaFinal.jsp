<%-- 
    Document   : PagoTarjetaFinal
    Created on : 05-12-2019, 23:58:45
    Author     : Kevin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="tipopago" method="POST">
            <div class="jumbotron text-center">
                <a href="menuPrincipal.jsp">Volver al menu principal</a>
                <h1><FONT FACE="impact" color="orange">Tarjeta</FONT></h1>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <table class="table table-striped">
                            <tr>
                                <td>Rut: </td>
                                <td><input type="text" name="txtRut" class="form-control" value="${idRut}"  disabled=""></td>
                            </tr>
                            <tr>
                                <td>Numero Tarjeta: </td>
                                <td><input type="number" name="txtTarjeta" class="form-control" required min="0" max="2000000"></td>
                            </tr>

                            <tr>
                                <td>Numero seguridad:</td>
                                <td><input type="number" name="txtNumeroSeguridad" class="form-control" value="" required=""></td>
                            </tr>
                            <tr>
                                <td>Precio: </td>
                                <td><input type="text" name="txtPrecio" class="form-control" value="$20.000" disabled=""></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <center>
                <div class="container">
                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Pagar</button>

                    <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Datos del pago</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>Rut: ${idRut}</h4>
                                    <h4>N° Reserva: ${idReservaFinal}</h4>
                                    <h4>Valor total: $${total} </h4>
                                    <h4>Monto abonado: $20.000</h4>
                                    <h4>Valor restante: $${totalConAbono}</h4>
                                </div>
                                <div class="modal-footer">
                                    <button name="btnAccion" value="Pagar" id="GuardarIN" class="btn btn-success">Guardar</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </center>
        </form>
    </body>
</html>
