
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Gran Turismo| Tus Reservas</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href="layout/styles/layoutL.css" rel="stylesheet" type="text/css" media="all">    
    </head>
    <body id="top">
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- Top Background Image Wrapper -->
        <div class="bgded overlay light" style="background-image:url('images/demo/backgrounds/01.jpeg');"> 
            <!-- ################################################################################################ -->
            <div class="wrapper row0">
                <div id="topbar" class="hoc clear"> 
                    <!-- ################################################################################################ -->
                    <ul class="nospace">
                        <li><a href="menuPrincipal.jsp"><i class="fa fa-lg fa-home"></i></a></li>
                        <li><a href="cerrarSesion.jsp" title="Cerrar sesion"><i class="fa fa-lg fa-edit"></i></a></li>
                    </ul>
                    <!-- ################################################################################################ -->
                </div>
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div class="wrapper row1">
                <header id="header" class="hoc clear"> 
                    <!-- ################################################################################################ -->
                    <div id="logo" class="fl_left">
                        <h1><a href="menuPrincipal.jsp">Gran Turismo</a></h1>
                    </div>
                    <nav id="mainav" class="fl_right">
                        <ul class="clear">
                            <li><a href="menuPrincipal.jsp">Home</a></li>
                            <li class="active"><a class="drop" href="#">¡Reserva Ahora!</a>
                                <ul>
                                    <li><a href="departamentosDisponibles.jsp">Departamenos</a></li>
                                    <li class="active"><a href="listaReservas.jsp">Tus reservas</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                    <!-- ################################################################################################ -->
                </header>
            </div>
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <!-- ################################################################################################ -->
            <div id="breadcrumb" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <ul>
                    <li><a href="menuPrincipal.jsp">Home</a></li>
                    <li><a href="listaReservas.jsp">Reservas</a></li>
                </ul>
                <!-- ################################################################################################ -->
            </div>
            <!-- ################################################################################################ -->
        </div>
        <!-- End Top Background Image Wrapper -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row3">
            <main class="hoc container clear"> 
                <!-- main body -->
                <!-- ################################################################################################ -->
                <div class="content"> 
                    <!-- ################################################################################################ -->
                    <h1>Mira tus reservas!</h1>
                    <img class="imgr borderedbox inspace-5" src="images/demo/departamento.gif">
                    <p>Bienvenido al menu de reservas, en esta seccion podras revisar tu reservas hechas y reservas ya realizadas en otra ocasion</p>
                    <p>¡Importante! Si no tienes ninguna reserva realizada en esta seccion no aparecera nada a tu cuenta de usuario</p>
                    <img class="imgl borderedbox inspace-5" src="images/demo/departamento2.gif" alt="">
                    <p>En turismo real trabajamos para ti, recuerda que solo podras observar el estado de tu reserva, excluye poder modificarla</p>
                    <p>¡Consejo!: Si por algun error tu reserva no concuerda con lo datos que ingreso, ponte en contacto con nosotros un ejecutivo te atendera para solucionarlo</p>
                    <p>Por ultimo: Esperamos que disfrutes la nueva plataforma de Turismo Real, comparte y cuenta tu experiencia a tus amigos, gracias por prefeirnos y hagamos que esto sea una experiencia una para ti</p>
                    <h1>Reservas</h1>
                    <div class="scrollable">
                        <table>
                            <thead>
                                <tr>
                                    <th>N° Reserva</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Fecha Salida </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="item" items="${listaFinal}">
                                    <tr>
                                        <td>${item.getIdReserva()}</td>
                                        <td>${item.getFechaIngreso()}</td>
                                        <td>${item.getFechaSalida()}</td>
                                <form action="" method="">
                                    <td><input type="submit" name="btnAccion" value="Detalle" disabled="" class="btn btn-info"></td>
                                </form>
                            </c:forEach>
                            </tbody>
                        </table>
                        <form action="reserva" method="POST">
                            <input type="submit" name="btnAccion" value="Tus reservas!" class="btn btn-success">
                        </form>
                    </div>
                    <!-- ################################################################################################ -->
                </div>
                <!-- ################################################################################################ -->
                <!-- / main body -->
                <div class="clear"></div>
            </main>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row4 ">
            <footer id="footer" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <article class="one_quarter first">
                    <h6 class="heading">Gran Turismo</h6>
                    <p>Fundada en el año 2019</p>
                    <p>Turismo Real, empresa que sirve a la comunidad su principal objetivo es dar a sus cliente una experiencia inolvidable en las reservas que hagan a travez de la nueva plataforma "Guia del turista"</p>
                </article>
                <div class="one_quarter">
                    <h6 class="heading">Contactanos</h6>
                    <ul class="nospace btmspace-30 linklist contact">
                        <li><i class="fa fa-map-marker"></i>
                            <address>
                                Pasaje la armonica &amp; 01385, Puente alto, Santiago
                            </address>
                        </li>
                        <li><i class="fa fa-phone"></i> +56(9) 32140169</li>
                        <li><i class="fa fa-envelope-o"></i> granturismo@gmail.com</li>
                    </ul>
                </div>
                <!-- ################################################################################################ -->
            </footer>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <div class="wrapper row5">
            <div id="copyright" class="hoc clear"> 
                <!-- ################################################################################################ -->
                <p class="fl_left">Copyright &copy; 2019 - All Rights Reserved </p>
                <!-- ################################################################################################ -->
            </div>
        </div>
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <!-- ################################################################################################ -->
        <a id="backtotop" href="#top"><i class="fa fa-chevron-up"></i></a>
        <!-- JAVASCRIPTS -->
        <script src="layout/scripts/jqueryL.min.js"></script>
        <script src="layout/scripts/jqueryL.backtotop.js"></script>
        <script src="layout/scripts/jqueryL.mobilemenu.js"></script>
    </body>
</html>
