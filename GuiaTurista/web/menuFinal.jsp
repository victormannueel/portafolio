<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="jumbotron text-center">
            <a href="menuPrincipal.jsp">Volver al menu principal</a>
            <h1><FONT FACE="impact" color="orange">Datos de la reserva</FONT></h1>
            <h3><FONT FACE="impact" color="orange">Garcias por reservar ${user}!!</FONT></h3>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <center>
                        <h4>Rut: ${idRut}</h4>
                        <br>
                        <h4>N° Reserva: ${idReservaFinal}</h4>
                        <br>
                        <h4>Valor total: $${total} </h4>
                        <br>
                        <h4>Monto abonado: $20.000</h4>
                        <br>
                        <h4>Valor restante: $${totalConAbono}</h4>
                        <br>
                        <h4>Forma de pago: Tarjeta</h4>
                    </center>
                </div>
            </div>
        </div>

    </body>
</html>
