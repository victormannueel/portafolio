
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <style>
            @import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
            @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);

            body{
                margin: 0;
                padding: 0;
                background: #fff;

                color: #fff;
                font-family: Arial;
                font-size: 12px;
            }

            .body{
                position: absolute;
                top: -20px;
                left: -20px;
                right: -40px;
                bottom: -40px;
                width: auto;
                height: auto;
                background-image: url(http://ginva.com/wp-content/uploads/2012/07/city-skyline-wallpapers-008.jpg);
                background-size: cover;
                -webkit-filter: blur(5px);
                z-index: 0;
            }

            .grad{
                position: absolute;
                top: -20px;
                left: -20px;
                right: -40px;
                bottom: -40px;
                width: auto;
                height: auto;
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
                z-index: 1;
                opacity: 0.7;
            }

            .header{
                position: absolute;
                top: calc(50% - 35px);
                left: calc(50% - 255px);
                z-index: 2;
            }

            .header div{
                float: left;
                color: #fff;
                font-family: 'Exo', sans-serif;
                font-size: 35px;
                font-weight: 200;
            }

            .header div span{
                color: #5379fa !important;
            }
        .box {
            position: absolute;
            top: 65%;
            left: 50%;
            transform: translate(-50%,-50%);
            width: 400px;
            padding: 40px;
            background: rgba(0,0,0,.8);
            box-sizing: border-box;
            box-shadow: 0 15px 25px rgba(0,0,0,.5);
            border-radius: 10px;
        }
        .box .avatar {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            position: absolute;
            top: -50px;
            left: calc(50% - 50px);
        }
        .box h2{
            margin: 0 0 30px;
            padding: 0;
            color: #fff;
            text-align: center;
        }
        .box .inputBox{
            position: relative;
        }
        .box .inputBox input{
            width: 100%;
            padding: 5px 0;
            font-size: 16px;
            color: #fff;
            letter-spacing: 1px;
            margin-bottom: 30px;
            border: none;
            border-bottom: 1px solid #fff;
            outline: none;
            background: transparent;
        }
        .box .inputBox label {
            position: absolute;
            top: 0;
            left: 0;
            padding: 10px 0;
            font-size: 16px;
            color: #fff;
            pointer-events: none;
            transition: .5s;
        }
        .box .inputBox input:focus ~ label,
        .box .inputBox input:valid ~ label
        {
            top: -20px;
            left: 0;
            color: #03a9f4;
            font-size: 10px;
        }
        .box input[type="submit"]{
            width: 100%;
            background: transparent;
            border: none;
            outline: none;
            color: #fff;
            background: #03a9f4;
            padding: 10px 20px;
            cursor: pointer;
            border-radius: 5px;
            margin-top: 5%;
        }
        .box .inputBox i{
            position: absolute;
            top: 0;
            right: 0;
            padding: 5px 0;
            font-size: 5px;
            color: #fff;
            pointer-events: none;
            transition: .5s;
        }
        .box .inputBox a{
            position: absolute;
            top: 0;
            right: 0;
            text-align: right;
        }
        </style>
        <script src="js/prefixfree.min.js"></script>
    </head>
    <body>

    </div>
    <div class="body"></div>
    <div class="box">
        <img class="avatar" src="../images/logo.png">
        <h2>Registrarse</h2>
    <form action="../usuario" method="POST">
        <div class="inputBox">
            <input type="text" name="txtRut" id="txtUsuario" required="" minlength="5" maxlength="15">
            <label>Rut</label>
        </div>
        <div class="inputBox">
            <input type="text" name="txtNombre" id="txtUsuario" required="" minlength="3" maxlength="20">
            <label>Nombre</label>
        </div>
        <div class="inputBox">
            <input type="text" name="txtApellido" id="txtUsuario" required="" minlength="3" maxlength="20">
            <label>Apellido</label>
        </div>
        <div class="inputBox">
            <input type="email" name="txtEmail" id="txtUsuario" required="" minlength="12" maxlength="30">
            <label>Email</label>
        </div>
        <div class="inputBox">
            <input type="number"  name="txtNumero" id="txtUsuario" required="" min="8" minlength="10">
            <label>Telefono</label>
        </div>
        <div class="inputBox">
            <input type="date"  name="txtFecha" id="txtUsuario" required=""  min="1980-01-01" max="2001-12-31">
        </div>
        <div class="inputBox">
            <input type="text" name="txtUser" id="txtUsuario" required="" minlength="2" maxlength="20">
            <label>Username</label>
        </div>
        <div class="inputBox">
            <input type="password" name="txtPass" id="txtUsuario" required="" minlength="2" maxlength="20">
            <label>Password</label>
        </div>
        <div>
                <input type="submit" name="btnAccion"  value="Registrar">
        </div>
        <br>
        <div>
            <c:forEach var="item" items="${exito}">
                <div class="alert alert-success alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Correcto!</strong> ${item}
                </div>
            </c:forEach>
            <c:forEach var="item" items="${errorUsu}">
                <div class="alert alert-danger alert-dismissible fade in">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Error!</strong> ${item}
                </div>
            </c:forEach>
        </div>
        <br>
        <div>
            <a href="../login.jsp">Iniciar Sesión</a>
        </div>
    </form>
    </div>
    <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
</body>
</html>
