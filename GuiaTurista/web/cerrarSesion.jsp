<%-- 
    Document   : cerrarSesion
    Created on : 27-11-2019, 15:04:41
    Author     : Kevin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            HttpSession sesion = request.getSession();
            sesion.setAttribute("user", "");
            sesion.setAttribute("pass", "");

            sesion.invalidate();
            response.sendRedirect("login.jsp");
        %>
    </body>
</html>
