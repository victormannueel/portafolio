/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "RESERVA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r")
    , @NamedQuery(name = "Reserva.findByIdReserva", query = "SELECT r FROM Reserva r WHERE r.idReserva = :idReserva")
    , @NamedQuery(name = "Reserva.findByFechaIngreso", query = "SELECT r FROM Reserva r WHERE r.fechaIngreso = :fechaIngreso")
    , @NamedQuery(name = "Reserva.findByFechaSalida", query = "SELECT r FROM Reserva r WHERE r.fechaSalida = :fechaSalida")
    , @NamedQuery(name = "Reserva.findByNumAcompanantes", query = "SELECT r FROM Reserva r WHERE r.numAcompanantes = :numAcompanantes")})
public class Reserva implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_RESERVA")
    private int idReserva;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_SALIDA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaSalida;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUM_ACOMPANANTES")
    private int numAcompanantes;
    @JoinColumn(name = "ID_ABONO", referencedColumnName = "ID_ABONO")
    @ManyToOne(optional = false)
    private Abono idAbono;
    @JoinColumn(name = "ID_DEPARTAMENTO", referencedColumnName = "ID_DEPARTAMENTO")
    @ManyToOne(optional = false)
    private Departamento idDepartamento;
    @JoinColumn(name = "ID_ESTADO_RESERVA", referencedColumnName = "ID_ESTADO_RESERVA")
    @ManyToOne(optional = false)
    private EstadoReserva idEstadoReserva;
    @JoinColumn(name = "ID_SERVICIO_EXTRA", referencedColumnName = "ID_SERVICIO_EXTRA")
    @ManyToOne(optional = false)
    private ServicioExtra idServicioExtra;
    @JoinColumn(name = "RUT_USUARIO", referencedColumnName = "RUT_USUARIO")
    @ManyToOne(optional = false)
    private Usuario rutUsuario;

    public Reserva() {
    }

    public Reserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public Reserva(EstadoReserva idEstadoReserva) {
        this.idEstadoReserva = idEstadoReserva;
    }

    public Reserva(int idReserva, Date fechaIngreso, Date fechaSalida, int numAcompanantes) {
        this.idReserva = idReserva;
        this.fechaIngreso = fechaIngreso;
        this.fechaSalida = fechaSalida;
        this.numAcompanantes = numAcompanantes;
    }

    public int getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public int getNumAcompanantes() {
        return numAcompanantes;
    }

    public void setNumAcompanantes(int numAcompanantes) {
        this.numAcompanantes = numAcompanantes;
    }

    public Abono getIdAbono() {
        return idAbono;
    }

    public void setIdAbono(Abono idAbono) {
        this.idAbono = idAbono;
    }

    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Departamento idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public EstadoReserva getIdEstadoReserva() {
        return idEstadoReserva;
    }

    public void setIdEstadoReserva(EstadoReserva idEstadoReserva) {
        this.idEstadoReserva = idEstadoReserva;
    }

    public ServicioExtra getIdServicioExtra() {
        return idServicioExtra;
    }

    public void setIdServicioExtra(ServicioExtra idServicioExtra) {
        this.idServicioExtra = idServicioExtra;
    }

    public Usuario getRutUsuario() {
        return rutUsuario;
    }

    public void setRutUsuario(Usuario rutUsuario) {
        this.rutUsuario = rutUsuario;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idReserva != null ? idReserva.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Reserva)) {
//            return false;
//        }
//        Reserva other = (Reserva) object;
//        if ((this.idReserva == null && other.idReserva != null) || (this.idReserva != null && !this.idReserva.equals(other.idReserva))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "Clases.Reserva[ idReserva=" + idReserva + " ]";
    }
    
}
