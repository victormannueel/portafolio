/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "TARJETA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarjeta.findAll", query = "SELECT t FROM Tarjeta t")
    , @NamedQuery(name = "Tarjeta.findByIdTarjeta", query = "SELECT t FROM Tarjeta t WHERE t.idTarjeta = :idTarjeta")
    , @NamedQuery(name = "Tarjeta.findByRut", query = "SELECT t FROM Tarjeta t WHERE t.rut = :rut")
    , @NamedQuery(name = "Tarjeta.findByNumeroTarjeta", query = "SELECT t FROM Tarjeta t WHERE t.numeroTarjeta = :numeroTarjeta")
    , @NamedQuery(name = "Tarjeta.findByNumeroSeguridad", query = "SELECT t FROM Tarjeta t WHERE t.numeroSeguridad = :numeroSeguridad")
    , @NamedQuery(name = "Tarjeta.findByPrecio", query = "SELECT t FROM Tarjeta t WHERE t.precio = :precio")})
public class Tarjeta implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TARJETA")
    private int idTarjeta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "RUT")
    private String rut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUMERO_TARJETA")
    private int numeroTarjeta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NUMERO_SEGURIDAD")
    private int numeroSeguridad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRECIO")
    private int precio;

    public Tarjeta() {
    }

    public Tarjeta(int idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public Tarjeta(String rut, int numeroTarjeta, int numeroSeguridad, int precio) {
        this.rut = rut;
        this.numeroTarjeta = numeroTarjeta;
        this.numeroSeguridad = numeroSeguridad;
        this.precio = precio;
    }

    
    
    public Tarjeta(int idTarjeta, String rut, int numeroTarjeta, int numeroSeguridad, int precio) {
        this.idTarjeta = idTarjeta;
        this.rut = rut;
        this.numeroTarjeta = numeroTarjeta;
        this.numeroSeguridad = numeroSeguridad;
        this.precio = precio;
    }

    public int getIdTarjeta() {
        return idTarjeta;
    }

    public void setIdTarjeta(int idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(int numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public int getNumeroSeguridad() {
        return numeroSeguridad;
    }

    public void setNumeroSeguridad(int numeroSeguridad) {
        this.numeroSeguridad = numeroSeguridad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idTarjeta != null ? idTarjeta.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Tarjeta)) {
//            return false;
//        }
//        Tarjeta other = (Tarjeta) object;
//        if ((this.idTarjeta == null && other.idTarjeta != null) || (this.idTarjeta != null && !this.idTarjeta.equals(other.idTarjeta))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "Clases.Tarjeta[ idTarjeta=" + idTarjeta + " ]";
    }
    
}
