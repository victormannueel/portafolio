/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "ESTADO_RESERVA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoReserva.findAll", query = "SELECT e FROM EstadoReserva e")
    , @NamedQuery(name = "EstadoReserva.findByIdEstadoReserva", query = "SELECT e FROM EstadoReserva e WHERE e.idEstadoReserva = :idEstadoReserva")
    , @NamedQuery(name = "EstadoReserva.findByDescipcion", query = "SELECT e FROM EstadoReserva e WHERE e.descipcion = :descipcion")})
public class EstadoReserva implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ESTADO_RESERVA")
    private int idEstadoReserva;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "DESCIPCION")
    private String descipcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEstadoReserva")
    private List<Reserva> reservaList;

    public EstadoReserva() {
    }

    public EstadoReserva(int idEstadoReserva) {
        this.idEstadoReserva = idEstadoReserva;
    }

    public EstadoReserva(int idEstadoReserva, String descipcion) {
        this.idEstadoReserva = idEstadoReserva;
        this.descipcion = descipcion;
    }

    public int getIdEstadoReserva() {
        return idEstadoReserva;
    }

    public void setIdEstadoReserva(int idEstadoReserva) {
        this.idEstadoReserva = idEstadoReserva;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }

    @XmlTransient
    public List<Reserva> getReservaList() {
        return reservaList;
    }

    public void setReservaList(List<Reserva> reservaList) {
        this.reservaList = reservaList;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (idEstadoReserva != null ? idEstadoReserva.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof EstadoReserva)) {
//            return false;
//        }
//        EstadoReserva other = (EstadoReserva) object;
//        if ((this.idEstadoReserva == null && other.idEstadoReserva != null) || (this.idEstadoReserva != null && !this.idEstadoReserva.equals(other.idEstadoReserva))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "Clases.EstadoReserva[ idEstadoReserva=" + idEstadoReserva + " ]";
    }
    
}
