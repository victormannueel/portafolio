/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kevin
 */
@Entity
@Table(name = "CARGO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cargo.findAll", query = "SELECT c FROM Cargo c")
    , @NamedQuery(name = "Cargo.findByIdTipoCargo", query = "SELECT c FROM Cargo c WHERE c.idTipoCargo = :idTipoCargo")
    , @NamedQuery(name = "Cargo.findByDescripcion", query = "SELECT c FROM Cargo c WHERE c.descripcion = :descripcion")})
public class Cargo implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TIPO_CARGO")
    private BigDecimal idTipoCargo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoCargo")
    private List<Sesion> sesionList;

    public Cargo() {
    }

    public Cargo(BigDecimal idTipoCargo) {
        this.idTipoCargo = idTipoCargo;
    }

    public Cargo(BigDecimal idTipoCargo, String descripcion) {
        this.idTipoCargo = idTipoCargo;
        this.descripcion = descripcion;
    }

    public BigDecimal getIdTipoCargo() {
        return idTipoCargo;
    }

    public void setIdTipoCargo(BigDecimal idTipoCargo) {
        this.idTipoCargo = idTipoCargo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Sesion> getSesionList() {
        return sesionList;
    }

    public void setSesionList(List<Sesion> sesionList) {
        this.sesionList = sesionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoCargo != null ? idTipoCargo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cargo)) {
            return false;
        }
        Cargo other = (Cargo) object;
        if ((this.idTipoCargo == null && other.idTipoCargo != null) || (this.idTipoCargo != null && !this.idTipoCargo.equals(other.idTipoCargo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Clases.Cargo[ idTipoCargo=" + idTipoCargo + " ]";
    }
    
}
