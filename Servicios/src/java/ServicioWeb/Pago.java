/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServicioWeb;

import Clases.Tarjeta;
import DAO.PagoDao;
import java.sql.SQLException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Kevin
 */
@WebService(serviceName = "Pago")
public class Pago {


    @WebMethod(operationName = "pagotarjeta")
    public boolean pagotarjeta(@WebParam(name = "rut") String rut, @WebParam(name = "numero_tarjeta") int numero_tarjeta, @WebParam(name = "numero_seguridad") int numero_seguridad,  @WebParam(name = "precio") int precio)  {
        Tarjeta lista = new Tarjeta(rut, numero_tarjeta, numero_seguridad,precio);
        PagoDao dao = new PagoDao();
        try {
            dao.agregarConsumidor(lista);
            return true;
        } catch (Exception e) {

        }
        return false;
    }
}
