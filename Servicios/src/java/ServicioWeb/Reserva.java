/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServicioWeb;

import Clases.Departamento;
import Clases.EstadoReserva;
import Conexion.Conexion;
import DAO.EstadoReservaDao;
import DAO.ReservaDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Kevin
 */
@WebService(serviceName = "Reserva")
public class Reserva {

    Conexion cone = new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    private Connection conexion;

    /**
     * Web service operation
     */
    @WebMethod(operationName = "mostrarReservaFinal")
    public Clases.Reserva[] mostrarReservaFinal(@WebParam(name = "rut") String rut) {
        Clases.Reserva[] lista = null;
        ReservaDao dao = new ReservaDao();
        try {
            lista = dao.mostrarReserva(rut);
        } catch (Exception ex) {
        }
        return lista;
    }

    @WebMethod(operationName = "mostrarDepaPorId")
    public Departamento[] mostrarDepaPorId(@WebParam(name = "id") int id) {
        Departamento[] lista = null;
        ReservaDao dao = new ReservaDao();
        try {
            lista = dao.reservaSegunIdDepa(id);
        } catch (Exception e) {
        }
        return lista;
    }

    @WebMethod(operationName = "estadoReservaFinal")
    public EstadoReserva[] estadoReservaFinal(@WebParam(name = "rut") int id) {
        EstadoReserva[] lista = null;
        EstadoReservaDao dao = new EstadoReservaDao();
        try {
            lista = dao.moestrarEstadoReserva(id);
        } catch (Exception ex) {
        }
        return lista;
    }

    public int recuperarNumeroReserva(@WebParam(name = "rut") String rut) {
        int numeroReserva = 0;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM RESERVA WHERE RUT_USUARIO = '" + rut + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                numeroReserva = resultados.getInt(1);
                return numeroReserva;
            } else {
                String mensaje = "Usuario t/o contraseña incorrectos";
                return 0;
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return 0;
    }
//    public String fechaIngreso(@WebParam(name = "rut") String rut) {
//        String ingresoFecha = null;
//        try {
//            this.conexion = new Conexion().obtenerConexion();
//            ResultSet rs = null;
//            String query = "SELECT * FROM RESERVA WHERE RUT_USUARIO = '" + rut + "'";
//            ResultSet resultados = conexion.createStatement().executeQuery(query);
//            if (resultados.next()) {
//                ingresoFecha = resultados.getString(2);
//                return ingresoFecha;
//            } else {
//                String mensaje = "Usuario t/o contraseña incorrectos";
//                return null;
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return null;
//    }
//
//    public String fechaSalida(@WebParam(name = "rut") String rut) {
//        String fechaSalida = null;
//        try {
//            this.conexion = new Conexion().obtenerConexion();
//            ResultSet rs = null;
//            String query = "SELECT * FROM RESERVA WHERE RUT_USUARIO = '" + rut + "'";
//            ResultSet resultados = conexion.createStatement().executeQuery(query);
//            if (resultados.next()) {
//                fechaSalida = resultados.getString(3);
//                return fechaSalida;
//            } else {
//                String mensaje = "Usuario t/o contraseña incorrectos";
//                return null;
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return null;
//    }
//
//    public int acompanantes(@WebParam(name = "rut") String rut) {
//        int acompanantes = 0;
//        try {
//            this.conexion = new Conexion().obtenerConexion();
//            ResultSet rs = null;
//            String query = "SELECT * FROM RESERVA WHERE RUT_USUARIO = '" + rut + "'";
//            ResultSet resultados = conexion.createStatement().executeQuery(query);
//            if (resultados.next()) {
//                acompanantes = resultados.getInt(4);
//                return acompanantes;
//            } else {
//                String mensaje = "Usuario t/o contraseña incorrectos";
//                return 0;
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return 0;
//    }
//
//    public int servicioExtra(@WebParam(name = "rut") String rut) {
//        int servicio = 0;
//        try {
//            this.conexion = new Conexion().obtenerConexion();
//            ResultSet rs = null;
//            String query = "SELECT * FROM RESERVA WHERE RUT_USUARIO = '" + rut + "'";
//            ResultSet resultados = conexion.createStatement().executeQuery(query);
//            if (resultados.next()) {
//                servicio = resultados.getInt(5);
//                return servicio;
//            } else {
//                String mensaje = "Usuario t/o contraseña incorrectos";
//                return 0;
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return 0;
//    }
//

    public String estadoReservaA(@WebParam(name = "rut") String rut) {
        String estado = null;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT ES.ID_ESTADO_RESERVA,ES.DESCIPCION FROM RESERVA R  JOIN ESTADO_RESERVA ES ON R.ID_ESTADO_RESERVA = ES.ID_ESTADO_RESERVA WHERE R.RUT_USUARIO ='" + rut + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                estado = resultados.getString("DESCIPCION");
                return estado;
            } else {
                String mensaje = "Usuario t/o contraseña incorrectos";
                return null;
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }

    public int abonoReserva(@WebParam(name = "rut") String rut) {
        int abono = 0;
        try {
            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT A.ID_ABONO, A.PRECIO FROM RESERVA R  JOIN ABONO A ON R.ID_ABONO = A.ID_ABONO WHERE R.RUT_USUARIO = '" + rut + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                abono = resultados.getInt("PRECIO");
                return abono;
            } else {
                String mensaje = "Usuario t/o contraseña incorrectos";
                return 0;
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return 0;
    }
//
//    public int depa(@WebParam(name = "rut") String rut) {
//        int depa = 0;
//        try {
//            this.conexion = new Conexion().obtenerConexion();
//            ResultSet rs = null;
//            String query = "SELECT * FROM RESERVA WHERE RUT_USUARIO = '" + rut + "'";
//            ResultSet resultados = conexion.createStatement().executeQuery(query);
//            if (resultados.next()) {
//                depa = resultados.getInt(9);
//                return depa;
//            } else {
//                String mensaje = "Usuario t/o contraseña incorrectos";
//                return 0;
//            }
//        } catch (Exception e) {
//            e.getMessage();
//        }
//        return 0;
//    }
    /**
     * Web service operation
     */
}
