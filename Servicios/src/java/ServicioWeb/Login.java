/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServicioWeb;

import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Kevin
 */
@WebService(serviceName = "Login")
public class Login {

    Conexion cone = new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    Connection con;
    Conexion conex = new Conexion();
    private Connection conexion;

    @WebMethod(operationName = "ingresar")
    public Boolean ingresar(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        //TODO write your implementation code here:
        try {
            this.conexion = new Conexion().obtenerConexion();
            String query = "SELECT * FROM SESION WHERE USUARIO  ='" + user + "' AND PASS ='" + pass + "' AND ID_ESTADO_USU = '" + 1 + "'";
            CallableStatement cstmt = this.conexion.prepareCall(query);

            if (cstmt.executeUpdate() > 0) {

                return true;
            } else {

                String mensaje = "usuario y/o contraseña incorrectos";
                return false;
            }

        } catch (Exception e) {

            e.getMessage();
        }

        return null;
    }

    public int recuperarID(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        //RECUPERAR EL ID DE LA SESUIB PARA LUEGO TRABAJAR CON LAS SESIONES EN LA WEB
        int usuId = 0;

        try {

            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM SESION WHERE USUARIO  ='" + user + "' AND PASS ='" + pass + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                usuId = Integer.parseInt(resultados.getString(1));
                return usuId;
            } else {

                String mensaje = "usuario y/o contraseña incorrectos";
                return 0;
            }

        } catch (Exception e) {

            e.getMessage();
        }
        return usuId;

    }

    public int recuperarTipo(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        //RECUPERAR EL TIPO DE LA SESION PARA LUEGO TRABAJAR CON LAS SESIONES EN LA WEB
        int usuTipo = 0;
        try {

            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM SESION WHERE USUARIO  ='" + user + "' AND PASS ='" + pass + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                usuTipo = Integer.parseInt(resultados.getString(5));
                return usuTipo;
            } else {

                String mensaje = "usuario y/o contraseña incorrectos";
                return 0;
            }

        } catch (Exception e) {

            e.getMessage();
        }

        return 0;
    }

    public int recuperarTipoUsu(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        //RECUPERAR EL TIPO DE LA SESION PARA LUEGO TRABAJAR CON LAS SESIONES EN LA WEB
        int usuTipo = 0;
        try {

            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM SESION WHERE USUARIO  ='" + user + "' AND PASS ='" + pass + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                usuTipo = Integer.parseInt(resultados.getString(6));
                return usuTipo;
            } else {

                String mensaje = "usuario y/o contraseña incorrectos";
                return 0;
            }

        } catch (Exception e) {

            e.getMessage();
        }

        return 0;
    }

    public String recuperarRut(@WebParam(name = "user") String user, @WebParam(name = "pass") String pass) {
        //RECUPERAR EL RUT DE LA SESION PARA LUEGO TRABAJAR CON LAS SESIONES EN LA WEB
        String usuTipo = null;
        try {

            this.conexion = new Conexion().obtenerConexion();
            ResultSet rs = null;
            String query = "SELECT * FROM SESION WHERE USUARIO  ='" + user + "' AND PASS ='" + pass + "'";
            ResultSet resultados = conexion.createStatement().executeQuery(query);
            if (resultados.next()) {
                usuTipo = resultados.getString(4);
                return usuTipo;
            } else {

                String mensaje = "usuario y/o contraseña incorrectos";
                return null;
            }

        } catch (Exception e) {

            e.getMessage();
        }

        return null;
    }

}
