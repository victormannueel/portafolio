/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Clases.Departamento;
import Clases.EstadoReserva;
import Clases.Reserva;
import Clases.Usuario;
import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kevin
 */
public class ReservaDao {

    Reserva reserva, usuarioFinal;
    Departamento departamento;
    ResultSet rs;
    PreparedStatement st;
    String query;
    String[] arr;
    List LaLista;
    Usuario u;
    private Connection conexion;

    public Reserva[] mostrarReserva(String rut) throws ClassNotFoundException {
        try {
            this.conexion = new Conexion().obtenerConexion();
            LaLista = new ArrayList();
            query = "SELECT * FROM RESERVA R JOIN ESTADO_RESERVA ES ON R.ID_ESTADO_RESERVA = ES.ID_ESTADO_RESERVA WHERE R.RUT_USUARIO = '" + rut + "'";
            st = conexion.prepareStatement(query);
            rs = st.executeQuery();
            while (rs.next()) {
                reserva = new Reserva();
                reserva.setIdReserva(rs.getInt("ID_RESERVA"));
                reserva.setFechaIngreso(rs.getDate("FECHA_INGRESO"));
                reserva.setFechaSalida(rs.getDate("FECHA_SALIDA"));
                reserva.setNumAcompanantes(rs.getInt("NUM_ACOMPANANTES"));
                LaLista.add(reserva);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (Reserva[]) LaLista.toArray(new Reserva[LaLista.size()]);
    }

    public Departamento[] reservaSegunIdDepa(int id) throws ClassNotFoundException {
        try {
            this.conexion = new Conexion().obtenerConexion();
            LaLista = new ArrayList();
            query = "SELECT * FROM DEPARTAMENTO d JOIN RESERVA r ON r.ID_DEPARTAMENTO = d.ID_DEPARTAMENTO WHERE r.ID_RESERVA = '" + id + "'";
            st = conexion.prepareStatement(query);
            rs = st.executeQuery();
            while (rs.next()) {
                departamento = new Departamento();
                departamento.setIdDepartamento(rs.getInt("ID_DEPARTAMENTO"));
                departamento.setNombre(rs.getString("NOMBRE"));
                departamento.setDescripcion(rs.getString("DESCRIPCION"));
                departamento.setNumHabitacion(rs.getInt("NUM_HABITACION"));
                departamento.setPrecioNoche(rs.getInt("PRECIO_NOCHE"));
                departamento.setImagen(rs.getString("IMAGEN"));
                departamento.setCantHabitaciones(rs.getInt("CANT_HABITACIONES"));

//                reserva.setIdEstadoReserva((EstadoReserva) rs.getObject("ID_ESTADO_RESERVA"));
                LaLista.add(departamento);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (Departamento[]) LaLista.toArray(new Departamento[LaLista.size()]);
    }

}
