/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Clases.Tarjeta;
import Conexion.Conexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Kevin
 */
public class PagoDao {

    private Connection conexion;

    public boolean agregarConsumidor(Tarjeta con) throws SQLException {
        boolean bandera = false;
        try {
            this.conexion = new Conexion().obtenerConexion();
            String llamadaProcedimiento = "{ call SP_REGISTRAR_PAGO(?,?,?,?)}";
            CallableStatement cstmt = this.conexion.prepareCall(llamadaProcedimiento);
            cstmt.setString(1, con.getRut());
            cstmt.setInt(2, con.getNumeroTarjeta());
            cstmt.setInt(3, con.getNumeroSeguridad());
            cstmt.setInt(4, con.getPrecio());
            if (cstmt.executeUpdate() > 0) {
                bandera = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.conexion.close();
        }
        return bandera;
    }
}
