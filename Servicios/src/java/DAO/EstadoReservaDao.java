/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Clases.EstadoReserva;
import Clases.Reserva;
import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Kevin
 */
public class EstadoReservaDao {

    EstadoReserva reserva;
    ResultSet rs;
    PreparedStatement st;
    String query;
    String[] arr;
    List LaLista;
    List servicio;
    private Connection conexion;

    public EstadoReserva[] moestrarEstadoReserva(int id) throws ClassNotFoundException {
        try {
            this.conexion = new Conexion().obtenerConexion();
            LaLista = new ArrayList();
            servicio = new ArrayList();
            query = "SELECT ES.ID_ESTADO_RESERVA, ES.DESCIPCION FROM ESTADO_RESERVA ES JOIN RESERVA RE ON ES.ID_ESTADO_RESERVA = RE.ID_ESTADO_RESERVA WHERE RE.ID_ESTADO_RESERVA = '" + id + "'";
            st = conexion.prepareStatement(query);
            rs = st.executeQuery();
            while (rs.next()) {
                reserva = new EstadoReserva();
                reserva.setIdEstadoReserva(rs.getInt("ID_ESTADO_RESERVA"));
                reserva.setDescipcion(rs.getString("DESCIPCION"));
                LaLista.add(reserva);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (EstadoReserva[]) LaLista.toArray(new EstadoReserva[LaLista.size()]);
    }
}
